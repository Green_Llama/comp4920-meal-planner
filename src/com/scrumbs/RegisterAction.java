package com.scrumbs;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.UUID;
import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.commons.lang.StringUtils;

import com.scrumbs.data.UnconfirmedDAO;
import com.scrumbs.data.UnconfirmedDTO;
import com.scrumbs.data.UserDAO;
import com.scrumbs.data.UserDTO;
import com.scrumbs.util.EmailSender;

public class RegisterAction extends WebAction {
	String username;
	String password;
	String email;
	String confirm;
	String firstname;
	String lastname;

	public String execute() throws SQLException, AddressException, MessagingException {
		
		UUID s = UUID.randomUUID();
		String code = s.toString();
		UserDAO userDao = new UserDAO();
		UserDTO newUser;
		Date date = new Date();
		Timestamp time = new Timestamp(date.getTime());
		
		if (username == null
				|| password == null
				|| email == null
				|| confirm == null
				|| firstname == null) {
			alerts.add("MISSING_FIELD");
			return "failure";
		}
		
		// Validation
		// Check if username is valid
		if (username.length() == 0 || !StringUtils.isAlphanumeric(username.replace('_', 'X')) || username.length() > 32) {
			alerts.add("INVALID_USERNAME");
			return "failure";
		}
		// Check if username exists
		if (userDao.getUser(username) != null) {
			alerts.add("USER_EXISTS");
			return "failure";
		}
		// Check if passwords match
		if (!password.equals(confirm)) {
			alerts.add("PASSWORDS_DO_NOT_MATCH");
			return "failure";
		}
		// Check if passwords are strong
		if (password.length() < 5 || password.length() > 64) {
			alerts.add("BAD_PASSWORD");
			return "failure";
		}
		
		// Check if names are too long
		if (firstname.length() > 255 || firstname.length() == 0 || lastname.length() > 255) {
			alerts.add("The name you supplied is too long. Sorry, Scandinavia.");
		}
		
		// Check if email is valid
		if (!email.contains("@") && email.length() < 256) {
			/* We use a loose email restriction because the email RFC is really complicated.
			 * Most email regular expressions are too restrictive, and are not RFC-compliant.
			 * Email strings are a very expressive language that is almost impossible to parse
			 * via regexp. I have, for example, a friend that has the email " "@<domain name>.
			 * It is really, really annoying if your email is denied for being invalid when
			 * it isn't.
			 * 
			 * Sending a verification email will tell us nice and clear if the email exists.
			 */
			alerts.add("BAD_EMAIL");
			return "failure";
		}
		// Check if email is registered
		newUser = userDao.getUser(email); 
		if (newUser != null && newUser.getEmail_confirmed()) {
			alerts.add("USER_EXISTS");
			return "failure";
		}
		
		// TODO: Delete email records if unverified.

		newUser = userDao.store(username, password, email, firstname, lastname);

		UnconfirmedDTO new_code = new UnconfirmedDTO(newUser.getUser_id(), code, time);
		new UnconfirmedDAO().store(new_code);
		
		// get userid in database then send e-mail
		EmailSender test = new EmailSender();
		// try {
			test.send(username, email, code, request);
		//} catch (Exception e) {
			//alerts.add("BAD_EMAIL");
			//return "failure";
		//}
		alerts.add("EMAIL_SENT");
		return "success";

	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getConfirm() {
		return confirm;
	}

	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

}
