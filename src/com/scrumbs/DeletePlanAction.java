package com.scrumbs;

import java.sql.SQLException;

import com.scrumbs.data.PlanEntryDAO;
import com.scrumbs.data.WeeklyPlanDAO;
import com.scrumbs.data.WeeklyPlanDTO;

public class DeletePlanAction extends WebAction {
	public Integer plan_id;
	
	public String execute() throws SQLException {
		if (loggedin) {
			if (plan_id != null) {
				WeeklyPlanDAO wpdao = new WeeklyPlanDAO();
				WeeklyPlanDTO plan = wpdao.getPlan(plan_id);
				if (plan != null && plan.getUser().getUser_id() == user.getUser_id()) {
					PlanEntryDAO pedao = new PlanEntryDAO();
					pedao.deleteAll(plan.getPlan_id());
					wpdao.delete(plan.getPlan_id());
				} else {
					alerts.add("Invalid weekly plan ID.");
				}
				return "success";
			}
		}
		return "404";
	}

	public Integer getPlan_id() {
		return plan_id;
	}

	public void setPlan_id(Integer plan_id) {
		this.plan_id = plan_id;
	}
	
}
