package com.scrumbs;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.scrumbs.data.PlanEntryDAO;
import com.scrumbs.data.PlanEntryDTO;
import com.scrumbs.data.WeeklyPlanDAO;
import com.scrumbs.data.WeeklyPlanDTO;

/**
 * Save a user plan.
 * 
 * @author Joey Tuong (joey@tetrap.us)
 *
 */
public class SavePlanAction extends WebAction {
	String name;
	public WeeklyPlanDTO plan;
	
	public String execute() throws SQLException {
		Map<String, String[]> parameters = request.getParameterMap();
		WeeklyPlanDAO wpdao = new WeeklyPlanDAO();
		PlanEntryDAO pedao = new PlanEntryDAO();
		Date currentTime = new Date();
		Timestamp t = new Timestamp(currentTime.getTime());
		if (loggedin && name != null) {

			// Store weekly plan.
		    plan = wpdao.store(user.getUser_id(), name, t);
			Pattern re = Pattern.compile("^entries\\[(.+)\\]\\[(.+)\\]$");
			
			for (Entry<String, String[]> e : parameters.entrySet()) {
				Matcher m = re.matcher(e.getKey());
				if (m.find()) {
					try {
						PlanEntryDTO p = new PlanEntryDTO(plan.plan_id, Integer.parseInt(e.getValue()[0]), m.group(1), m.group(2));
						pedao.store(p);
					} catch (NumberFormatException n) { }
				}
			}
			return "success";
		}
		return "404";
 	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
