package com.scrumbs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.scrumbs.data.RecipeDAO;
import com.scrumbs.data.RecipeDTO;


public class HomePageAction extends WebAction {

	public List<RecipeDTO> recipes;

	public String execute() throws Exception {
		RecipeDAO rdao = new RecipeDAO();
		List<RecipeDTO> results = new ArrayList<RecipeDTO>();
		for (RecipeDTO r : rdao.findAll()) {
			if (r.getRating() != null && r.getRating() > 3) {
				results.add(r);
			} else if (loggedin && r.getRating() == null) {
				results.add(r);
			}
		}
		Collections.shuffle(results);
		recipes = results.subList(0, 3);

		return "success";
	}

	
	// HERES SOME SAMPLE JAVA CODE TO INSERT/RETRIEVE STUFF FROM THE DATABASE:
	//userDTO  test= new userDTO("Suphawit","Ice","Suphawit","Sapp","suphawit.gmail.com","1993","1111");
	//userDAO test2 =new userDAO();
	//Iterator it = test2.findAll().iterator();
	//while(it.hasNext()){
	//	 	userDTO z = (userDTO)it.next();
	//	 	System.out.println(z.getFirstname());
	//}
	//test2.store(test);

	//recipeDTO testr = new recipeDTO("food","Suphawit","a","a","a","a","a","a","a","a");
	//recipeDAO test2r= new recipeDAO();
	//test2r.findAll();
	//Iterator it2 = test2r.findAll().iterator();
	///while(it2.hasNext()){
	///	recipeDTO z = (recipeDTO)it2.next();
	//	 	System.out.println(z.getTitle());
	//}
	//test2r.store(testr);
}