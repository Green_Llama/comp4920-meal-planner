package com.scrumbs;

import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;

import com.scrumbs.data.UserDAO;
import com.scrumbs.data.UserDTO;

public class EditAction extends ProfileAction {
	String firstname;
	String lastname;
	String o_password;
	String password;
	String c_password;
	String new_username;

	public String execute() {
		try {
			if (loggedin) {
				int user_id = user.getUser_id();

				// Check input
				if (password == null || o_password == null
						|| c_password == null || firstname == null) {
					System.out.println("null field missing");
					alerts.add("MISSING_FIELD");
					super.execute();
					return "failure";
				}
				if ((!(password.equals(c_password)))) {
					System.out.println("check password");
					alerts.add("PASSWORDS_DO_NOT_MATCH");
					super.execute();
					return "failure";
				}
				if (!(user.verifyPassword(o_password))) {
					System.out.println("wrong old password");
					alerts.add("PASSWORDS_DO_NOT_MATCH");
					super.execute();
					return "failure";
				}
				if (password.length() > 0 && password.length() < 5
						|| password.length() > 64) {
					System.out.println("password error");
					alerts.add("BAD_PASSWORD");
					super.execute();
					return "failure";
				}
				UserDAO userDAO = new UserDAO();
				if (!new_username.toLowerCase().equals(user.getUsername().toLowerCase())) {
					// Check if username is valid
					if (new_username.length() == 0 || !StringUtils.isAlphanumeric(new_username.replace('_', 'X')) || new_username.length() > 32) {
						alerts.add("INVALID_USERNAME");
						super.execute();
						return "failure";
					}
					// Check if username exists
					if (userDAO.getUser(new_username) != null) {
						alerts.add("USER_EXISTS");
						super.execute();
						return "failure";
					}
					userDAO.setUsername(user.getUser_id(), new_username);
				}

				// Edit the database fields.
				if (password.length() > 0) {
					// Set the password.
					userDAO.changePassword(user_id, password);
				}
				userDAO.setName(user_id, firstname, lastname);

			}
		} catch (SQLException e) {
			// TODO
			e.printStackTrace();
		}
		return "success";
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getO_password() {
		return o_password;
	}

	public void setO_password(String o_password) {
		this.o_password = o_password;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getC_password() {
		return c_password;
	}

	public void setC_password(String c_password) {
		this.c_password = c_password;
	}

	public String getLastname() {
		return firstname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getNew_username() {
		return new_username;
	}

	public void setNew_username(String new_username) {
		this.new_username = new_username;
	}
}
