package com.scrumbs;

import java.sql.SQLException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;

import com.scrumbs.data.SessionDAO;
import com.scrumbs.data.SessionDTO;
import com.scrumbs.data.UserDAO;
import com.scrumbs.data.UserDTO;

public class LoginAction extends WebAction {

	String username;
	String password;
	
	/**
	 * @param args
	 * @throws SQLException 
	 */
	public String execute() throws SQLException {
		// Check username & password
		UserDAO userDao = new UserDAO();
		UserDTO checkUser = userDao.getUser(username);
		SessionDAO sessDao = new SessionDAO();
		SessionDTO session;
       		
		if (loggedin) {
			return "404"; // How did they get here?
		}
		
		if (checkUser == null) {
			// user does not exist
			alerts.add("LOGIN_FAIL");
			return "failure";
		}
		if (checkUser.verifyPassword(password) && checkUser.getEmail_confirmed()) {
			// Set cookies
			session = sessDao.store(checkUser.getUser_id());
			response.addCookie(new Cookie("sessionid", session.getSessionid()));
			loggedin = true;
			user = checkUser;
			return "success";
		} else {
			// Wrong password
			alerts.add("LOGIN_FAIL");
			return "failure";
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
