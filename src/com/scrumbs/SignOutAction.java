package com.scrumbs;

import java.sql.SQLException;

import com.scrumbs.data.SessionDAO;

public class SignOutAction extends WebAction {

	public String execute() {
		try {
			SessionDAO sess = new SessionDAO();
			if (loggedin) {
				// Delete cookie
				sess.removeSession(session.getSessionid());
				loggedin = false;
				user = null;
				session = null;
			}
		} catch (SQLException e) {
			// TODO
			e.printStackTrace();
		}
		return "success";
	}
	
}
