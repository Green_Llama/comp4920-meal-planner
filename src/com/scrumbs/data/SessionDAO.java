package com.scrumbs.data;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.scrumbs.util.Security;

public class SessionDAO extends ObjDAO<SessionDTO> {
	static Logger logger = Logger.getLogger(UserDAO.class.getName());

	
	public SessionDAO() throws SQLException {
		super();
		// TODO
	}

	@Override
	public List<SessionDTO> findAll() throws SQLException {
		return null;
	}
	
	/**
	 * Generate a session entry for the user.
	 * 
	 * @param user_id
	 * @return
	 * @throws SQLException 
	 */
	public SessionDTO store(int user_id) throws SQLException {
		// Create a cookie
		String cookie = Security.generateSalt();
		Date date = new Date();
		Timestamp time = new Timestamp(date.getTime());

		try {
			connect();
			stmnt = connection
					.prepareStatement("INSERT INTO sessions (user_id, cookie, timestamp) VALUES (?, ?, ?)");
			stmnt.setInt(1, user_id);
			stmnt.setString(2, cookie);
			stmnt.setTimestamp(3, time);

			int result = stmnt.executeUpdate();

			logger.info("Statement successfully executed " + result);
			
			return new SessionDTO(user_id, cookie, time);

		} finally {
			close();
		}
	}
	
	public SessionDTO getSession(String cookie) throws SQLException {
		try {
			connect();
			stmnt = connection
					.prepareStatement("SELECT * FROM sessions WHERE cookie=?");
			stmnt.setString(1, cookie);
			rs = stmnt.executeQuery();
			if (rs != null && rs.next()) {
				return new SessionDTO(rs.getInt("user_id"), cookie, rs.getTimestamp("timestamp"));
			} else {
				return null;
			}
		} finally {
			close();
		}
	}
	
	public void removeSession(String cookie) throws SQLException {
		try {
			connect();
			stmnt = connection
					.prepareStatement("DELETE FROM sessions WHERE cookie=?");
			stmnt.setString(1, cookie);
			stmnt.executeUpdate();
		} finally {
			close();
		}
	}

}
