package com.scrumbs.data;

import java.sql.SQLException;
import java.sql.Timestamp;

public class SessionDTO {
	UserDTO user;
	String sessionid;
	Timestamp timestamp;
	
	public SessionDTO(int user_id, String cookie, Timestamp time) throws SQLException {
		UserDAO udao = new UserDAO();
		
		this.user = udao.getUser(user_id);
		this.sessionid = cookie;
		this.timestamp = time;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	
}
