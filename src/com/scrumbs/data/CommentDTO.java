package com.scrumbs.data;

import java.sql.SQLException;
import java.sql.Timestamp;

public class CommentDTO {
	
	int recipe;
	UserDTO user;
	String text;
	Timestamp timestamp;
	
	
	public CommentDTO(int recipe, int user, String text, Timestamp timestamp) throws SQLException {
		super();
		UserDAO udao = new UserDAO();
		this.recipe = recipe;
		this.user = udao.getUser(user);
		this.text = text;
		this.timestamp = timestamp;
	}


	public int getRecipe() {
		return recipe;
	}


	public void setRecipe(int recipe) {
		this.recipe = recipe;
	}


	public UserDTO getUser() {
		return user;
	}


	public void setUser(UserDTO user) {
		this.user = user;
	}


	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Timestamp getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}


	/**
	 * @param args
	 */


}
