package com.scrumbs.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.scrumbs.util.Security;

public class UserDAO extends ObjDAO<UserDTO> {
	static Logger logger = Logger.getLogger(UserDAO.class.getName());

	public UserDAO() throws SQLException {
		super();
	}

	public UserDTO store(String username, String password, String email,
			String firstname, String lastname) throws SQLException {
		// Create a salt
		String salt = Security.generateSalt();

		try {
			connect();
			stmnt = connection
					.prepareStatement("INSERT INTO users (username, password, salt, email, email_confirmed, firstname, lastname) VALUES (?, ?, ?, ?, ?, ?, ?)");
			stmnt.setString(1, username);
			stmnt.setString(2, Security.getHash(password, salt));
			stmnt.setString(3, salt);
			stmnt.setString(4, email);
			stmnt.setBoolean(5, false);
			stmnt.setString(6, firstname);
			stmnt.setString(7, lastname);

			int result = stmnt.executeUpdate();

			logger.info("Statement successfully executed " + result);

			return getUser(username);
		} finally {
			close();
		}
	}

	@Override
	public List<UserDTO> findAll() throws SQLException {
		List<UserDTO> userList = new ArrayList<UserDTO>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM users");
			rs = stmnt.executeQuery();

			while (rs.next()) {
				userList.add(makeUser(rs));
			}

			logger.info("Fetched users");
			return userList;
		} finally {
			close();
		}
	}

	public UserDTO getUser(int id) throws SQLException {
		try {
			connect();
			stmnt = connection
					.prepareStatement("SELECT * FROM users WHERE user_id=?");
			stmnt.setInt(1, id);
			rs = stmnt.executeQuery();
			if (rs != null && rs.next()) {
				return makeUser(rs);
			} else {
				return null;
			}
		} finally {
			close();
		}
	}

	public int getUserID(String username) throws SQLException {
		try {
			connect();
			stmnt = connection
				.prepareStatement("SELECT user_id FROM users WHERE username=?");
			stmnt.setString(1, username);
			rs = stmnt.executeQuery();
			if (rs.next()) {
				return rs.getInt("user_id");
			} else {
				return -1;
			}
		} finally {
			close();
		}
	}

	public void updateActivatedStatus(int id) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("UPDATE users SET email_confirmed=? WHERE user_id=?");
			stmnt.setBoolean(1, true);
			stmnt.setInt(2, id);
			stmnt.executeUpdate();
		} finally {
			close();
		}
	}

	public UserDTO getUser(String userid) throws SQLException {
		try {
			connect();
			stmnt = connection
					.prepareStatement("SELECT * FROM users WHERE username=? OR email=?");
			stmnt.setString(1, userid);
			stmnt.setString(2, userid);
			rs = stmnt.executeQuery();
	
			if (rs != null && rs.next()) {
				return makeUser(rs);
			} else {
				return null;
			}
		} finally {
			close();
		}
	}

	private UserDTO makeUser(ResultSet rs) {
		try {
			return new UserDTO(rs.getInt("user_id"), rs.getString("username"),
					rs.getString("password"), rs.getString("salt"),
					rs.getString("email"), rs.getBoolean("email_confirmed"),
					rs.getString("firstname"), rs.getString("lastname"));
		} catch (SQLException e) {
			logger.severe("Could not make user from result set.");
			return null;
		}
	}
	
	public void setName(int userid, String firstname, String lastname) throws SQLException{
		try {
			connect();
			stmnt = connection.prepareStatement("UPDATE users SET firstname=?, lastname=? WHERE user_id=?");
			stmnt.setString(1, firstname);
			stmnt.setString(2, lastname);
			stmnt.setInt(3, userid);
	        stmnt.executeUpdate();			        
		} finally {
			close();
		}
	}


	public void changePassword(int userid, String password) throws SQLException {
		String salt = Security.generateSalt();
		try {
			connect();
			stmnt = connection.prepareStatement("UPDATE users SET password=?, salt=? WHERE user_id=?");
			stmnt.setString(1, Security.getHash(password, salt));
			stmnt.setString(2, salt);
			stmnt.setInt(3, userid);
	        stmnt.executeUpdate();			        
		} finally {
			close();
		}
	}

	public void setUsername(int user_id, String username) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("UPDATE users SET username=? WHERE user_id=?");
			stmnt.setString(1, username);
			stmnt.setInt(2, user_id);
	        stmnt.executeUpdate();			        
		} finally {
			close();
		}
	}
}
