package com.scrumbs.data;

import java.sql.SQLException;
import java.util.Map;

import com.scrumbs.util.Security;

public class UserDTO {

	int user_id;

	String username;
	String password;
	String salt;

	String email;
	Boolean email_confirmed;
	
	String firstname;
	String lastname;
	
	Map<Integer, Integer> ratings;
	
	public UserDTO(int user_id, String username, String password, String salt,
			String email, Boolean email_confirmed, String firstname,
			String lastname) {
		super();
		this.user_id = user_id;
		this.username = username;
		this.password = password;
		this.salt = salt;
		this.email = email;
		this.email_confirmed = email_confirmed;
		this.firstname = firstname;
		this.lastname = lastname;
		this.ratings = null;
	}

	public boolean verifyPassword(String test) {
		return password.equals(Security.getHash(test, salt));
	}
	
	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getEmail_confirmed() {
		return email_confirmed;
	}
	public void setEmail_confirmed(Boolean email_confirmed) {
		this.email_confirmed = email_confirmed;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Map<Integer, Integer> getRatings() {
		if (ratings == null) {
			try {
				ratings = new RatedDAO().getRatingsByUserCheap(this.user_id);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return ratings;
	}

	public void setRatings(Map<Integer, Integer> ratings) {
		this.ratings = ratings;
	}

}
