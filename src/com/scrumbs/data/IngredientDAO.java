package com.scrumbs.data;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class IngredientDAO extends ObjDAO<IngredientDTO> {

	static Logger logger = Logger.getLogger(UserDAO.class.getName());

	public IngredientDAO() throws SQLException {
		super();
	}
	
	public List<IngredientDTO> getIngredients(int recipeId) throws SQLException {
		List<IngredientDTO> ingredients = new ArrayList<IngredientDTO>();
		
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM ingredients WHERE recipe=?");
			stmnt.setInt(1, recipeId);
			rs = stmnt.executeQuery();
			while (rs.next()) {
				ingredients.add(new IngredientDTO(recipeId, rs.getString("name"), rs.getString("quantity")));
			}
			if (ingredients.size() == 0) {
				ingredients.add(new IngredientDTO(recipeId, "None", ""));
			}
			return ingredients;
		} finally {
			close();
		}
	}

	@Override
	public List<IngredientDTO> findAll() throws SQLException {
		return null;
	}
	
	public void store(IngredientDTO ingredients) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("INSERT INTO ingredients ("
					+ "recipe, name, quantity) "
					+ "VALUES (?, ?, ?)");
			
			stmnt.setInt(1, ingredients.getRecipe());
			stmnt.setString(2, ingredients.getName());
			stmnt.setString(3, ingredients.getQuantity());
			
			int result = stmnt.executeUpdate();
			logger.info("Statement successfully executed " + result);
		} finally {
			close();
		}
	}

	public void deleteAll(int recipe_id) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("DELETE FROM ingredients WHERE recipe=?");
			
			stmnt.setInt(1, recipe_id);
			
			int result = stmnt.executeUpdate();

			logger.info("Statement successfully executed " + result);			
		} finally {
			close();
		}
	}


}
