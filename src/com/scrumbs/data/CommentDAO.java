package com.scrumbs.data;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentDAO extends ObjDAO<CommentDTO>{

	public CommentDAO() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<CommentDTO> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}


	public List<CommentDTO> getComments(int recipeId) throws SQLException {
		List<CommentDTO> comments = new ArrayList<CommentDTO>();
		
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM comments WHERE recipe=? ORDER BY timestamp");
			stmnt.setInt(1, recipeId);
			rs = stmnt.executeQuery();
			while (rs.next()) {
				comments.add(new CommentDTO(recipeId, rs.getInt("user"), rs.getString("text"), rs.getTimestamp("timestamp")));
			}
			
			return comments;
		} finally {
			close();
		}
	}


	
	public void store(CommentDTO comment) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("INSERT INTO comments ("
					+ "recipe, user, text, timestamp) "
					+ "VALUES (?, ?, ?, ?)");
			
			stmnt.setInt(1, comment.getRecipe());
			stmnt.setInt(2, comment.getUser().getUser_id());
			stmnt.setString(3, comment.getText());
			stmnt.setTimestamp(4, comment.getTimestamp());
	
			int result = stmnt.executeUpdate();
		} finally {
			close();
		}
	}

	public void deleteAllByRecipe(int recipe_id) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("DELETE FROM comments WHERE recipe=?");
			
			stmnt.setInt(1, recipe_id);
			
			int result = stmnt.executeUpdate();

		} finally {
			close();
		}
	}

}
