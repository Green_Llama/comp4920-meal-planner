/**
 * 
 */
package com.scrumbs.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Joey Tuong (joey@tetrap.us)
 *
 */
public class WeeklyPlanDAO extends ObjDAO<WeeklyPlanDTO> {

	public WeeklyPlanDAO() throws SQLException {
		super();
		// TODO
	}

	public WeeklyPlanDTO store(int user, String name, Timestamp t) throws SQLException {
		try {
			connect();
			stmnt = connection
					.prepareStatement("INSERT INTO plans (user, name, timestamp) VALUES (?, ?, ?)",
							Statement.RETURN_GENERATED_KEYS);
			stmnt.setInt(1, user);
			stmnt.setString(2, name);
			stmnt.setTimestamp(3, t);

			if (stmnt.executeUpdate() > 0) {
				rs = stmnt.getGeneratedKeys();
				rs.next();
				int id = rs.getInt(1);
				return new WeeklyPlanDTO(id, new UserDAO().getUser(user), name, t);
			} else {
				return null;
			}
		} finally {
			close();
		}
	}
	
	public WeeklyPlanDTO getPlan(int id) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM plans WHERE plan_id=?");
			stmnt.setInt(1, id);
			rs = stmnt.executeQuery();
			
			if (rs.next()) {
				return makeWeeklyPlan(rs);
			} else {
				return null;
			}
		} finally {
			close();
		}
	}
	
	
	public List<WeeklyPlanDTO> getPlans(int user) throws SQLException {
		List<WeeklyPlanDTO> result = new ArrayList<WeeklyPlanDTO>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM plans WHERE user=? ORDER BY timestamp");
			stmnt.setInt(1, user);
			rs = stmnt.executeQuery();
			
			while (rs.next()) {
				result.add(makeWeeklyPlan(rs));
			}
			
			return result;
		} finally {
			close();
		}
	}
	
	private WeeklyPlanDTO makeWeeklyPlan(ResultSet rs) throws SQLException {
		UserDAO dao = new UserDAO();
		return new WeeklyPlanDTO(rs.getInt("plan_id"), 
				dao.getUser(rs.getInt("user")),
				rs.getString("name"),
				rs.getTimestamp("timestamp"));
	}

	@Override
	public List<WeeklyPlanDTO> findAll() throws SQLException {
		return null;
	}

	public void delete(int plan_id) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("DELETE FROM plans WHERE plan_id=?");
			stmnt.setInt(1, plan_id);
			stmnt.executeUpdate();
		} finally {
			close();
		}
	}

}
