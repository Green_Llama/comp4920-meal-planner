/**
 * 
 */
package com.scrumbs.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Joey Tuong (joey@tetrap.us)
 *
 */
public class PlanEntryDAO extends ObjDAO<PlanEntryDTO> {

	public PlanEntryDAO() throws SQLException {
		super();
		// TODO
	}

	public void store(PlanEntryDTO p) throws SQLException {

		try {
			connect();
			stmnt = connection
					.prepareStatement("INSERT INTO planentry (plan, recipe, day, meal) VALUES (?, ?, ?, ?)"+
									  "ON DUPLICATE KEY UPDATE recipe=VALUES(recipe)");
			stmnt.setInt(1, p.getOwner());
			stmnt.setInt(2, p.getRecipe());
			stmnt.setString(3, p.getDay());
			stmnt.setString(4, p.getMeal());
			
			stmnt.executeUpdate();
		} finally {
			close();
		}
	}
	
	public List<PlanEntryDTO> getEntriesByRecipe(int recipe_id) throws SQLException {
		List<PlanEntryDTO> result = new ArrayList<PlanEntryDTO>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM planentry WHERE recipe=?");
			stmnt.setInt(1, recipe_id);
			rs = stmnt.executeQuery();
			
			while (rs.next()) {
				result.add(makeDTO(rs));
			}
			
			return result;
		} finally {
			close();
		}
	}
	
	public List<PlanEntryDTO> getEntriesByUser(int user_id) throws SQLException {
		List<PlanEntryDTO> result = new ArrayList<PlanEntryDTO>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM planentry WHERE user=?");
			stmnt.setInt(1, user_id);
			rs = stmnt.executeQuery();
			
			while (rs.next()) {
				result.add(makeDTO(rs));
			}
			
			return result;
		} finally {
			close();
		}
	}
	
	public List<PlanEntryDTO> getEntries(int id) throws SQLException {
		List<PlanEntryDTO> result = new ArrayList<PlanEntryDTO>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM planentry WHERE plan=?");
			stmnt.setInt(1, id);
			rs = stmnt.executeQuery();
			
			while (rs.next()) {
				result.add(makeDTO(rs));
			}
			
			return result;
		} finally {
			close();
		}
	}
	
	@Override
	public List<PlanEntryDTO> findAll() throws SQLException {
		return null;
	}
	
	public PlanEntryDTO makeDTO(ResultSet rs) throws SQLException {
		return new PlanEntryDTO(rs.getInt("plan"), 
				rs.getInt("recipe"),
				rs.getString("day"),
				rs.getString("meal"));
	}

	public void deleteAll(int plan_id) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("DELETE FROM planentry WHERE plan=?");
			stmnt.setInt(1, plan_id);
			stmnt.executeUpdate();
		} finally {
			close();
		}
	}

	public void delete(Integer plan_id, String day, String meal) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("DELETE FROM planentry WHERE plan=? AND day=? AND meal=?");
			stmnt.setInt(1, plan_id);
			stmnt.setString(2, day);
			stmnt.setString(3, meal);
			stmnt.executeUpdate();
		} finally {
			close();
		}
	}

	public void deleteAllByRecipe(int recipe_id) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("DELETE FROM planentry WHERE recipe=?");
			stmnt.setInt(1, recipe_id);
			stmnt.executeUpdate();
		} finally {
			close();
		}
	}

}
