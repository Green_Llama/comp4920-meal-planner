package com.scrumbs.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.scrumbs.util.Security;

public class SavedDAO extends ObjDAO<SavedDTO> {
	static Logger logger = Logger.getLogger(UserDAO.class.getName());

	
	public SavedDAO() throws SQLException {
		super();
		// TODO
	}

	@Override
	public List<SavedDTO> findAll() throws SQLException {
		
		List<SavedDTO> savedList = new ArrayList<SavedDTO>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM saved");
			rs = stmnt.executeQuery();

			while (rs.next()) {
				savedList.add(makeSaved(rs));
			}

			logger.info("Fetched users");
			return savedList;
		} finally {
			close();
		}
	}
	
	/**
	 * Generate a session entry for the user.
	 * 
	 * @param user_id
	 * @return
	 * @throws SQLException 
	 */
	public SavedDTO store(int user_id, int recipe_id) throws SQLException {
		// Create a cookie
		

		try {
			connect();
			stmnt = connection
					.prepareStatement("INSERT INTO saved (user, recipe) VALUES (?, ?)");
			stmnt.setInt(1, user_id);
			stmnt.setInt(2, recipe_id);
			
			int result = stmnt.executeUpdate();

			logger.info("Statement successfully executed " + result);
			
			return new SavedDTO(user_id, recipe_id);

		} finally {
			close();
		}
	}
	
		
	
	private SavedDTO makeSaved(ResultSet rs) {
		try {
			return new SavedDTO(rs.getInt("user_id"), rs.getInt("recipe_id"));
		} catch (SQLException e) {
			logger.severe("Could not make save from result set.");
			return null;
		}
	}
	public boolean isSaved(int user_id, int recipe_id) throws SQLException{
		try{
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM saved WHERE user= ? AND recipe=?");
			stmnt.setInt(1,user_id);
			stmnt.setInt(2, recipe_id);
			
		rs = stmnt.executeQuery();
		if(rs.next()== false){
			return false;
		}
		return true;
		}finally{
			close();
		}
	}
	
	public void unSave(int user_id, int recipe_id) throws SQLException {
		try{
			connect();
			stmnt = connection.prepareStatement("DELETE FROM saved WHERE user=? AND recipe=?");
			stmnt.setInt(1,user_id);
			stmnt.setInt(2, recipe_id);
			
			stmnt.execute();
		} finally {
			close();
		}
	}
	
	public List<Integer> getAllSaved(int user_id) throws SQLException
	{
		List<Integer> SavedRecipeList = new ArrayList<Integer>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT recipe FROM saved WHERE user =?");
			stmnt.setInt(1,user_id);
			rs = stmnt.executeQuery();

			while (rs.next()) {
				SavedRecipeList.add(rs.getInt("recipe"));
			}

			logger.info("Fetched users");
			return SavedRecipeList;
		} finally {
			close();
		}
	}

	public void deleteAllByRecipe(int recipe_id) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("DELETE FROM saved WHERE recipe=?");
			stmnt.setInt(1, recipe_id);
			stmnt.executeUpdate();
		} finally {
			close();
		}
	}
}

