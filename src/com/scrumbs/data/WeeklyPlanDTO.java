package com.scrumbs.data;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;

public class WeeklyPlanDTO {
	public int plan_id;
	public UserDTO user;
	public String name;
	public Timestamp timestamp;
	public HashMap<String, HashMap<String, RecipeDTO>> entries;

	public WeeklyPlanDTO(int plan_id, UserDTO user, String name,
			Timestamp timestamp) {
		super();
		this.plan_id = plan_id;
		this.user = user;
		this.name = name;
		this.timestamp = timestamp;
		this.entries = new HashMap<String, HashMap<String, RecipeDTO>>();
		populateEntries();
	}
	
	public WeeklyPlanDTO(int plan_id, UserDTO user, String name,
			Timestamp timestamp, HashMap<String, HashMap<String, RecipeDTO>> entries) {
		super();
		this.plan_id = plan_id;
		this.user = user;
		this.name = name;
		this.timestamp = timestamp;
		this.entries = entries;
	}
	
	public void addEntry(PlanEntryDTO p) throws SQLException {
		RecipeDAO rdao = new RecipeDAO();
		if (!entries.containsKey(p.getDay())) {
			entries.put(p.getDay(), new HashMap<String, RecipeDTO>());
		}
		entries.get(p.getDay()).put(p.getMeal(), rdao.getRecipe(p.recipe));
	}
	
	public void populateEntries() {
		try {
			PlanEntryDAO dao = new PlanEntryDAO();
			for (PlanEntryDTO p : dao.getEntries(plan_id)) {
				addEntry(p);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int getPlan_id() {
		return plan_id;
	}
	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public HashMap<String, HashMap<String, RecipeDTO>> getEntries() {
		if (entries == null) {
			populateEntries();
		}
		return entries;
	}

	public void setEntries(HashMap<String, HashMap<String, RecipeDTO>> entries) {
		this.entries = entries;
	}
	
}
