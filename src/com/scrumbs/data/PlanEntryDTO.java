package com.scrumbs.data;

public class PlanEntryDTO {
	public int owner; // not really necessary but why not
	public int recipe;
	public String day;
	public String meal;
	
	public PlanEntryDTO(int owner, int recipe, String day, String meal) {
		super();
		this.owner = owner;
		this.recipe = recipe;
		this.day = day;
		this.meal = meal;
	}
	
	public int getOwner() {
		return owner;
	}
	public void setOwner(int owner) {
		this.owner = owner;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getMeal() {
		return meal;
	}
	public void setMeal(String meal) {
		this.meal = meal;
	}

	public int getRecipe() {
		return recipe;
	}

	public void setRecipe(int recipe) {
		this.recipe = recipe;
	}
	
}
