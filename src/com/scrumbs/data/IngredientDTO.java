package com.scrumbs.data;

public class IngredientDTO {
	Integer recipe;
	String name;
	String quantity;
	
	public IngredientDTO() {
		super();
		recipe = null;
		name = null;
		quantity = null;
	}

	public IngredientDTO(Integer recipe, String name, String quantity) {
		super();
		this.recipe = recipe;
		this.name = name;
		this.quantity = quantity;
	}

	public int getRecipe() {
		return recipe;
	}

	public void setRecipe(int recipe) {
		this.recipe = recipe;
	}

	public String getName() {
		return name.trim();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQuantity() {
		return quantity.trim();
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	
}
