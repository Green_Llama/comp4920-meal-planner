package com.scrumbs.data;

import java.sql.SQLException;
import java.util.HashMap;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RatedDAO extends ObjDAO<RatedDTO> {

	public RatedDAO() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<RatedDTO> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Returns mappings from recipe -> rating for each recipe, by the given user
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public Map<RecipeDTO, RatedDTO> getRatedByUser(int userId) throws SQLException {
		Map<RecipeDTO, RatedDTO> ratedRecipeMap = new HashMap<RecipeDTO, RatedDTO>();
		RecipeDAO recipeDAO = new RecipeDAO();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM ratings WHERE user=?");
			stmnt.setInt(1, userId);
			rs = stmnt.executeQuery();
			
			while (rs.next()) {
				ratedRecipeMap.put(recipeDAO.getRecipe(rs.getInt("recipe")), new RatedDTO(rs.getInt("user"), rs.getInt("recipe"), rs.getInt("rating")));
			}
		} finally {
			close();
		}
		return ratedRecipeMap;
	}
	
	public Integer getRated(int userId, int recipeId) throws SQLException {
		try{
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM ratings WHERE user=? AND recipe=?");
			stmnt.setInt(1,userId);
			stmnt.setInt(2, recipeId);
			
			rs = stmnt.executeQuery();
			if (rs.next()== false){
				return null;
			}
			return rs.getInt("rating");
		} finally {
			close();
		}
	}
	
	public RatedDTO store(int user_id, int recipe_id, Integer rating) throws SQLException {
		// Create a cookie
		

		try {
			connect();
			stmnt = connection
					.prepareStatement("INSERT INTO ratings (user, recipe, rating) VALUES (?, ?, ?)");
			stmnt.setInt(1, user_id);
			stmnt.setInt(2, recipe_id);
			stmnt.setInt(3, rating);
			
			int result = stmnt.executeUpdate();

			
			return new RatedDTO(user_id, recipe_id, rating);

		} finally {
			close();
		}
	}
	
	public RatedDTO update(int user_id, int recipe_id, Integer rating) throws SQLException {
		try {
			connect();
			stmnt = connection
					.prepareStatement("UPDATE ratings SET rating=? WHERE user=? AND recipe=?");
			stmnt.setInt(1, rating);
			stmnt.setInt(2, user_id);
			stmnt.setInt(3, recipe_id);
			
			int result = stmnt.executeUpdate();

			
			return new RatedDTO(user_id, recipe_id, rating);

		} finally {
			close();
		}
	}
	public Double getAverageRating(int recipe_id) throws SQLException {
		
		double i = 0;
		double sum = 0;
		Double avgRating = null ;
		try{
			
			connect();
			stmnt = connection.prepareStatement("SELECT rating FROM ratings WHERE recipe=?");
			stmnt.setInt(1, recipe_id);
			rs = stmnt.executeQuery();	
			while (rs.next()) {
				sum += rs.getInt("rating");
				i++;
			}

			if (i > 0) {
				avgRating = sum/i;
			}
			
			return avgRating;
			
		} finally{
			close();
		}
	}

	public boolean hasRating(Integer recipe_id) throws SQLException {
		try{
					
					connect();
					stmnt = connection.prepareStatement("SELECT rating FROM ratings WHERE recipe=?");
					stmnt.setInt(1, recipe_id);
					rs = stmnt.executeQuery();
					if (rs.next()== false){
						return false;
					}
					return true;
		}			
		finally{
			close();
		}
	}

	public Map<Integer, Integer> getRatingsByUserCheap(int user_id) throws SQLException {
		Map<Integer, Integer> ratedRecipeMap = new HashMap<Integer, Integer>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM ratings WHERE user=?");
			stmnt.setInt(1, user_id);
			rs = stmnt.executeQuery();
			
			while (rs.next()) {
				ratedRecipeMap.put(rs.getInt("recipe"), rs.getInt("rating"));
			}
		} finally {
			close();
		}
		return ratedRecipeMap;
	}

	public void deleteAllByRecipe(int recipe_id) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("DELETE FROM ratings WHERE recipe=?");
			stmnt.setInt(1, recipe_id);
			stmnt.executeUpdate();
		} finally {
			close();
		}
	}

}
