package com.scrumbs.data;

import java.sql.SQLException;

public class RatedDTO {

	int user_id;
	int recipe_id;
	Integer rating;
	
	public RatedDTO(int user_id, int recipe_id, Integer rating) throws SQLException {
		this.user_id = user_id;
		this.recipe_id = recipe_id;
		this.rating = rating;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getRecipe_id() {
		return recipe_id;
	}

	public void setRecipe_id(int recipe_id) {
		this.recipe_id = recipe_id;
	}
	
	public Integer getRating() {
		return rating;
	}
	
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	
}
