package com.scrumbs.data;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class RecipeDTO {
    Integer recipe_id;
    String name; 
    String description;
    String picture;
    String method; 
    UserDTO author;
    Timestamp created; 
    String servings;
    String category; 
	String complexity;
    public Integer parent; // Tree structure: we want to do this lazily.
    public List<IngredientDTO> ingredients;
    public Double rating;
    public String ratingString;

    public RecipeDTO(Integer recipe_id, String name, String description,
			String picture, String method, UserDTO author, Timestamp created,
			String servings, String category, String complexity, Integer parent) {
		super();
    	IngredientDAO ingredientsdb;
    	RatedDAO ratingdb ;
		try {
			ingredientsdb = new IngredientDAO();
			this.ingredients = ingredientsdb.getIngredients(recipe_id);
			ratingdb= new RatedDAO();
			this.rating = ratingdb.getAverageRating(recipe_id);
			this.ratingString = String.format("%.1f", rating);
		} catch (SQLException e) {
			// TODO
			this.ingredients = new ArrayList<IngredientDTO>();
			this.ingredients.add(new IngredientDTO(1, "Fuck.", ""));
			this.rating = null;
			this.ratingString = "";
			e.printStackTrace();
		}
		this.recipe_id = recipe_id;
		this.name = name;
		this.description = description;
		this.picture = picture;
		this.method = method;
		this.author = author;
		this.created = created;
		this.servings = servings;
		this.category = category;
		this.complexity = complexity;
		this.parent = parent;
	}
    
    public String htmlMethod() {
    	return method.replace("\n", "<br />");
    }

	public Integer getRecipe_id() {
		return recipe_id;
	}
	public void setRecipe_id(Integer recipe_id) {
		this.recipe_id = recipe_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public UserDTO getAuthor() {
		return author;
	}
	public void setAuthor(UserDTO author) {
		this.author = author;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public String getServings() {
		return servings;
	}
	public void setServings(String servings) {
		this.servings = servings;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getComplexity() {
		return complexity;
	}
	public void setComplexity(String complexity) {
		this.complexity = complexity;
	}
	public Integer getParent() {
		return parent;
	}
	public void setParent(Integer parent) {
		this.parent = parent;
	}
	public List<IngredientDTO> getIngredients() {
		return ingredients;
	}
	public void setIngredients (List<IngredientDTO> ingredients) {
		this.ingredients = ingredients;
	}
	public Double getRating() {
		return rating;
	}
	public void setRating (Double rating) {
		this.rating = rating;
	}

	public String getRatingString() {
		return ratingString;
	}

	public void setRatingString(String ratingString) {
		this.ratingString = ratingString;
	}
}
