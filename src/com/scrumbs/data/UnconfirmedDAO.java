package com.scrumbs.data;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class UnconfirmedDAO extends ObjDAO<UnconfirmedDTO> {

	static Logger logger = Logger.getLogger(UserDAO.class.getName());

	public UnconfirmedDAO() throws SQLException {
		super();
	}

	public void store(UnconfirmedDTO code) throws SQLException {
		try {
			connect();
			stmnt = connection
					.prepareStatement("INSERT INTO unconfirmed (user_id,"
							+ "activation_code," + "timestamp) "
							+ "VALUES (?,?, ?)");
			stmnt.setInt(1, code.getUser_id());
			stmnt.setString(2, code.getActivation_code());
			stmnt.setTimestamp(3, code.getTime());

			int result = stmnt.executeUpdate();

			logger.info("Statement successfully executed " + result);
		} finally {
			close();
		}

	}

	@Override
	public List<UnconfirmedDTO> findAll() throws SQLException {
		List<UnconfirmedDTO> unconfirmedList = new ArrayList<UnconfirmedDTO>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM unconfirmed");
			rs = stmnt.executeQuery();

			while (rs.next()) {

				UnconfirmedDTO user = new UnconfirmedDTO(rs.getInt("user_id"),
						rs.getString("activation_code"),
						rs.getTimestamp("time"));
				unconfirmedList.add(user);
			}

			logger.info("Fetched comments");

		} finally {
			close();
		}
		return unconfirmedList;
	}

	public int getActivatingUserid(String code) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT user_id FROM unconfirmed WHERE activation_code=?");
			stmnt.setString(1, code);
			rs = stmnt.executeQuery();
			if (rs != null && rs.next()) {
				int userid = rs.getInt("user_id");
				return userid;
			} else {
				return -1;
			}
		} finally {
			close();
		}
	}
	
	public void cleanActivatedUser(String code) throws SQLException {
		try {
			connect();
			stmnt = connection
					.prepareStatement("DELETE FROM unconfirmed WHERE activation_code=?");
			stmnt.setString(1, code);
			stmnt.executeUpdate();
		} finally {
			close();
		}
	}

}
