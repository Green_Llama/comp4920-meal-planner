package com.scrumbs.data;

import java.sql.Timestamp;

public class UnconfirmedDTO {
	int user_id;
	String activation_code;
	Timestamp time;

	public UnconfirmedDTO() {
		activation_code = null;
		time = null;
	}

	public UnconfirmedDTO(int user_id, String activation_code, Timestamp time) {
		this.user_id = user_id;
		this.activation_code = activation_code;
		this.time = time;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getActivation_code() {
		return activation_code;
	}

	public void setActivation_code(String activation_code) {
		this.activation_code = activation_code;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

}
