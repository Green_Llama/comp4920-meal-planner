package com.scrumbs.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;



public abstract class ObjDAO<T> {
	public Connection connection = null;
	public PreparedStatement stmnt = null;
	public ResultSet rs = null;
	public DataSource ds;

	public ObjDAO() throws SQLException {
		Context context;
		try {
			context = new InitialContext();
			ds = (DataSource) context.lookup("java:comp/env/jdbc/db1");
		} catch (NamingException e) {
			e.printStackTrace();
		}

		System.out.println("Connecting...");
	}
	
	public void connect() throws SQLException {
		connection = ds.getConnection();
	}
	
	// The following utility functions are taken from http://balusc.blogspot.com.au/2008/07/dao-tutorial-data-layer.html.
	
	/**
     * Quietly close the Connection. Any errors will be printed to the stderr.
     * @param connection The Connection to be closed quietly.
     */
    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                System.err.println("Closing Connection failed: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /**
     * Quietly close the Statement. Any errors will be printed to the stderr.
     * @param statement The Statement to be closed quietly.
     */
    public static void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                System.err.println("Closing Statement failed: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /**
     * Quietly close the ResultSet. Any errors will be printed to the stderr.
     * @param resultSet The ResultSet to be closed quietly.
     */
    public static void close(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                System.err.println("Closing ResultSet failed: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }
	
    public void close() {
    	close(rs);
    	close(stmnt);
    	close(connection);
    }
    
	public abstract List<T> findAll() throws SQLException;	
	
}
