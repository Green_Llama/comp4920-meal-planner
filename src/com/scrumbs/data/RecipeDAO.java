package com.scrumbs.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class RecipeDAO extends ObjDAO<RecipeDTO> {

	static Logger logger = Logger.getLogger(UserDAO.class.getName());

	public RecipeDAO() throws SQLException {
		super();
	}
	
	public RecipeDTO store(String name, String description, String picture, String method, int author, Timestamp created, String servings, String category, String complexity, Integer parent) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("INSERT INTO recipes ("
					+ "name, description, picture, method, author, "
					+ "created, servings, category, complexity, parent) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			
			stmnt.setString(1, name);
			stmnt.setString(2, description);
			stmnt.setString(3, picture);
			stmnt.setString(4, method);
			stmnt.setInt(5, author);
			stmnt.setTimestamp(6, created);
			stmnt.setString(7, servings);
			stmnt.setString(8, category);
			stmnt.setString(9, complexity);
			if (parent == null) {
				stmnt.setNull(10, Types.NULL);
			} else {
				stmnt.setInt(10, parent);
			}
			
			int result = stmnt.executeUpdate();
			rs = stmnt.getGeneratedKeys();
			rs.next();
			int recipe_id = rs.getInt(1);
			logger.info("Statement successfully executed " + result);
			
			return getRecipe(recipe_id);
			
		} finally {
			close();
		}
	}
	
	public RecipeDTO update(int recipe_id, int user_id, String name, String description, String picture, String method, String servings, String category, String complexity) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("UPDATE recipes SET "
					+ "name=?, description=?, picture=?, method=?,"
					+ "servings=?, category=?, complexity=? WHERE recipe_id=? AND author=?");
			
			stmnt.setString(1, name);
			stmnt.setString(2, description);
			stmnt.setString(3, picture);
			stmnt.setString(4, method);
			stmnt.setString(5, servings);
			stmnt.setString(6, category);
			stmnt.setString(7, complexity);
			stmnt.setInt(8, recipe_id);
			stmnt.setInt(9, user_id);
			
			int result = stmnt.executeUpdate();

			logger.info("Statement successfully executed " + result);
			
			return getRecipe(recipe_id);
			
		} finally {
			close();
		}
	}
	
	public void setPicture(int recipe_id, String picture) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("UPDATE recipes SET picture=? WHERE recipe_id=?");
			
			stmnt.setString(1, picture);
			stmnt.setInt(2, recipe_id);
			
			int result = stmnt.executeUpdate();

			logger.info("Statement successfully executed " + result);			
		} finally {
			close();
		}	}
	
	public void unsetChildren(int parent_id) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("UPDATE recipes SET parent=null WHERE parent=?");
			
			stmnt.setInt(1, parent_id);
			
			int result = stmnt.executeUpdate();

			logger.info("Statement successfully executed " + result);			
		} finally {
			close();
		}
	}
	
	public void delete(int recipe_id) throws SQLException {
		unsetChildren(recipe_id);
		IngredientDAO idao = new IngredientDAO();
		idao.deleteAll(recipe_id);
		CommentDAO cdao = new CommentDAO();
		cdao.deleteAllByRecipe(recipe_id);
		PlanEntryDAO pdao = new PlanEntryDAO();
		pdao.deleteAllByRecipe(recipe_id);
		RatedDAO rdao = new RatedDAO();
		rdao.deleteAllByRecipe(recipe_id);
		SavedDAO sdao = new SavedDAO();
		sdao.deleteAllByRecipe(recipe_id);
		try {
			connect();
			stmnt = connection.prepareStatement("DELETE FROM recipes WHERE recipe_id=?");
			
			stmnt.setInt(1, recipe_id);
			
			int result = stmnt.executeUpdate();

			logger.info("Statement successfully executed " + result);			
		} finally {
			close();
		}
	}
	
	public List<RecipeDTO> findAll() throws SQLException {
		List<RecipeDTO> recipeList = new ArrayList<RecipeDTO>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM recipes");			
			rs = stmnt.executeQuery();
			
			while (rs.next()) {
				recipeList.add(makeRecipe(rs));
			}

			logger.info("Fetched recipes");
			
			return recipeList;
		} finally {
			close();
		}
	}
	
	public List<RecipeDTO> findRecipe(String substr) throws SQLException {
		List<RecipeDTO> recipeList = new ArrayList<RecipeDTO>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM recipes WHERE name LIKE ? OR description LIKE ? or category LIKE ? ");
			stmnt.setString(1, '%' + substr + '%'); // TODO: vulnerable to sqli
			stmnt.setString(2, '%' + substr + '%'); // TODO: vulnerable to sqli
			stmnt.setString(3, '%' + substr + '%'); // TODO: vulnerable to sqli
			
			rs = stmnt.executeQuery();
			
			while (rs.next()) {
				recipeList.add(makeRecipe(rs));
			}
	
			logger.info("Fetched recipess");
	
			return recipeList;
		} finally {
			close();
		}
	}
	
	public RecipeDTO getRecipe(int id) throws SQLException {
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM recipes WHERE recipe_id=?");
			stmnt.setInt(1, id);
			rs = stmnt.executeQuery();
			if (rs != null && rs.next()) {
				return makeRecipe(rs);

			} else {
				return null;
			}
		} finally {
			close();
		}
	}
	public List<RecipeDTO> getRecipesByAuthor(int id) throws SQLException {
		List<RecipeDTO> recipeList = new ArrayList<RecipeDTO>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM recipes WHERE author=?");
			stmnt.setInt(1, id);
			rs = stmnt.executeQuery();
			while (rs.next()) {
				recipeList.add(makeRecipe(rs));
			}
			return recipeList;
		} finally {
			close();
		}
	}
	
	public List<RecipeDTO> getRemixes(int id) throws SQLException {
		List<RecipeDTO> recipeList = new ArrayList<RecipeDTO>();
		try {
			connect();
			stmnt = connection.prepareStatement("SELECT * FROM recipes WHERE parent=?");
			stmnt.setInt(1, id);
			rs = stmnt.executeQuery();
			while (rs.next()) {
				recipeList.add(makeRecipe(rs));
			}
			return recipeList;
		} finally {
			close();
		}
	}
	
	private RecipeDTO makeRecipe(ResultSet rs) throws SQLException {
		
		UserDAO user = new UserDAO();
		return new RecipeDTO(rs.getInt("recipe_id"),
				 rs.getString("name"),
				 rs.getString("description"),
				 rs.getString("picture"),
				 rs.getString("method"),
				 user.getUser(rs.getInt("author")),
			     rs.getTimestamp("created"),
				 rs.getString("servings"),
				 rs.getString("category"),
				 rs.getString("complexity"),
				 rs.getInt("parent"));
		}
}
