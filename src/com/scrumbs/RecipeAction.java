package com.scrumbs;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.scrumbs.data.CommentDAO;
import com.scrumbs.data.CommentDTO;
import com.scrumbs.data.IngredientDTO;
import com.scrumbs.data.RatedDAO;
import com.scrumbs.data.RecipeDAO;
import com.scrumbs.data.RecipeDTO;
import com.scrumbs.data.SavedDAO;

public class RecipeAction extends WebAction {
	public Integer recipeId;
	public RecipeDTO recipe;
	public List<CommentDTO> comments;
	public boolean saved = false;
	public Integer rating;
	public String postComment;
	
	public RecipeDTO parent;
	public List<RecipeDTO> children;
	public List<RecipeDTO> similar;
	Map<Integer, Double> similarities;
	
	public String execute() throws SQLException {
		if (recipeId == null) {
			return "404";
		}
		CommentDAO commentDao = new CommentDAO();
		if (loggedin) {	
			SavedDAO savedDAO = new SavedDAO();
			saved = savedDAO.isSaved(user.getUser_id(), recipeId);	
			RatedDAO ratedDAO = new RatedDAO();
			rating = ratedDAO.getRated(user.getUser_id(), recipeId);
			
			if (postComment != null) {
				if (postComment.length() < 4096) {
					Date currentTime = new Date();
					Timestamp created = new Timestamp(currentTime.getTime());
					commentDao.store(new CommentDTO(recipeId, user.getUser_id(), postComment, created));
				} else {
					alerts.add("Comment too long.");
				}
			}
		} else if (postComment != null) {
			alerts.add("You must be logged in to add a comment.");
		}
		RecipeDAO recipeDao = new RecipeDAO();
		recipe = recipeDao.getRecipe(recipeId);
		comments = commentDao.getComments(recipeId);

		if (recipe == null) {
			return "404";
		} else {
			parent = recipeDao.getRecipe(recipe.parent);
			children = recipeDao.getRemixes(recipe.getRecipe_id());
		}
		
		// Calculate similar
		similar = recipeDao.findAll();
		similarities = new TreeMap<Integer, Double>();
		RecipeDTO current = null;
		for (RecipeDTO r : similar) {
			if (r.getRecipe_id() == recipe.getRecipe_id()) {
				current = r;
			} else {
				similarities.put(r.getRecipe_id(), similarity(recipe, r));
			}
		}
		System.out.println(similarities);
		similar.remove(current);
		Collections.sort(similar, new Comparator<RecipeDTO>() {

			@Override
			public int compare(RecipeDTO arg0, RecipeDTO arg1) {
				return -similarities.get(arg0.getRecipe_id()).compareTo(similarities.get(arg1.getRecipe_id()));
			}
			
		});
		List<RecipeDTO> similarCopy = new ArrayList<RecipeDTO>();
		
		for (RecipeDTO r : similar.subList(0, 7)) {
			if (similarities.get(r.getRecipe_id()) > 0.15) {
				similarCopy.add(r);
			}
		}
		similar = similarCopy;
		return "success";
	}
	
	private Double similarity(RecipeDTO r1, RecipeDTO r2) {
		// Similarity is calculated somewhat arbitrarily. It probably works well though?
		Set<String> ingredients1 = new HashSet<String>();
		for (IngredientDTO i : r1.getIngredients()) {
			ingredients1.add(i.getName().toLowerCase());
		}
		Set<String> ingredients2 = new HashSet<String>();
		for (IngredientDTO i : r2.getIngredients()) {
			ingredients2.add(i.getName().toLowerCase());
		}
		Double ingredient_score = intersectionScore(ingredients1, ingredients2);
		Double name_score = intersectionScore(Arrays.asList(r1.getName().toLowerCase().split("\\s+")), Arrays.asList(r2.getName().toLowerCase().split("\\s+")));
		return (ingredient_score + name_score)
								* (r1.getAuthor().getUser_id() == r2.getAuthor().getUser_id()? 1.0 : 0.87)
								* (r1.getParent() == r2.getParent()? 1.0 : 0.92);
	}
	
	private Double intersectionScore(Collection<String> x, Collection<String> y) {
		Set<String> xSet = new HashSet<String>(x);
		Set<String> ySet = new HashSet<String>(y);
		Set<String> intersection = new HashSet<String>(x);
		intersection.retainAll(y);
		return 2.0 * intersection.size() / (xSet.size() + ySet.size());
	}

	public Integer getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(Integer recipeId) {
		this.recipeId = recipeId;
	}

	public RecipeDTO getRecipe() {
		return recipe;
	}

	public void setRecipe(RecipeDTO recipe) {
		this.recipe = recipe;
	}

	public String getPostComment() {
		return postComment;
	}

	public void setPostComment(String comment) {
		this.postComment = comment;
	}

}
