package com.scrumbs;

import java.util.ArrayList;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.scrumbs.data.SessionDAO;
import com.scrumbs.data.SessionDTO;
import com.scrumbs.data.UserDTO;



/**
 * The baseclass handles sessions and user interactions.
 * 
 * @author Joey Tuong (joey@tetrap.us)
 *
 */
public class WebAction implements ServletResponseAware, ServletRequestAware {

	public Boolean loggedin = false;
	SessionDTO session = null;
	public UserDTO user = null;
	public ArrayList<String> alerts;
	public String alertlist;
	
	HttpServletRequest request;
	HttpServletResponse response;
	

	public WebAction() {
		try {
			SessionDAO dao = new SessionDAO();
			
			alerts = new ArrayList<String>();
			if (alertlist != null) {
				for (String alert : alertlist.split(",")) {
					alerts.add(alert);
				}
			}
			
			request = ServletActionContext.getRequest();
			// Check if login cookies are present
			if (request != null && request.getCookies() != null) {
				for (Cookie c : request.getCookies()) {
					if (c.getName().equals("sessionid")) {
						session = dao.getSession(c.getValue());
					}
				}
			}
			
			if (session != null) {
				loggedin = true;
				user = session.getUser();
			}
		} catch (Exception e) {
			System.out.println("Fuuuuuuuuck.");
			e.printStackTrace();
		}
	}
	
	public void redirectWithAlert(String alert) {
		// TODO
	}


	public String getAlertlist() {
		return alertlist;
	}


	public void setAlertlist(String alertlist) {
		this.alertlist = alertlist;
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}
}
