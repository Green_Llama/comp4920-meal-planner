package com.scrumbs;

import java.sql.SQLException;
import java.util.List;

import com.scrumbs.data.IngredientDTO;
import com.scrumbs.data.RecipeDAO;
import com.scrumbs.data.RecipeDTO;
import com.scrumbs.data.SavedDAO;
import com.scrumbs.data.SessionDAO;
import com.scrumbs.data.UserDTO;

public class FavoriteAction extends WebAction {
	Integer recipeId;
	RecipeDTO recipe;
	boolean saved;
	public String execute() throws SQLException {
		RecipeDAO rdao = new RecipeDAO();
		if (loggedin) {	
			recipe = rdao.getRecipe(recipeId);
			SavedDAO savedDAO = new SavedDAO();
			if (!savedDAO.isSaved(user.getUser_id(), recipeId)) {
				savedDAO.store(user.getUser_id(), recipeId);
			} else {
				savedDAO.unSave(user.getUser_id(), recipeId);
			}
			saved = savedDAO.isSaved(user.getUser_id(),recipeId);
		}
		//	RecipeDAO recipeDao = new RecipeDAO();
		//	recipe = recipeDao.getRecipe(recipeId);
		//	if (recipe == null) {
		//		return "404";
		//	}
		
		return "success";
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user =user;
	}

	public Integer getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(Integer recipeId) {
		this.recipeId = recipeId;
	}
	public RecipeDTO getRecipe() {
		return recipe;
	}

	public void setRecipe(RecipeDTO recipe) {
		this.recipe = recipe;
	}

}
