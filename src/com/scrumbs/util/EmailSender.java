package com.scrumbs.util;


import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
 
public class EmailSender {
	public void send(String name, String mail, String random, HttpServletRequest r) throws AddressException, MessagingException {
 
		final String username = "scrumbs@tetrap.us";
		final String password = "letmeiniwonthackyou";
 

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", Integer.toString(587));
 
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
 
			String email = mail;
			String url = r.getRequestURL().toString();
			url = url.replaceAll("/[^/]+$", "/confirm?activation_code=" + random);
			
			String content = "Dear " + name + ", " + "\n\n Thank you for creating an account on Scrumbs.\n Please click " 
			+ "on the link below to get started.\n";
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("scrumbs@tetrap.us"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			
			message.setSubject("Scrumbs: Confirm registration");
			message.setText(content + url);
			Transport.send(message);
 
			System.out.println("send Done");

	}
}