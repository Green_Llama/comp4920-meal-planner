package com.scrumbs;

import java.util.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.scrumbs.data.IngredientDTO;
import com.scrumbs.data.RatedDAO;
import com.scrumbs.data.RecipeDAO;
import com.scrumbs.data.RecipeDTO;

public class ResultsAction extends WebAction {
	private String searchQuery;
	private ArrayList<RecipeDTO> results;
	
	String author;
	String category;
	String complexity;
	String ingredients;
	String servings;
	String method;
	String created;
	String created_range;
	String rating;
	int recipe_id;
	
	public ResultsAction() {
		super();
		results = new ArrayList<RecipeDTO>();
	}

	public String execute() throws Exception {
		RecipeDAO recipeDao = new RecipeDAO();
		if (searchQuery == null) {
			searchQuery = "";
		}
		for (RecipeDTO item : recipeDao.findRecipe(searchQuery)) {
			// Maybe move some of these to the database for speed reasons?
		
			if (match(author, item.getAuthor().getUsername())
					&& match(category, item.getCategory())
					&& match(complexity, item.getComplexity())
					&& match(ingredients, item.getIngredients())
					&& match(servings, item.getServings())
					&& match(method, item.getMethod())
					&& match(rating, item.getRating())){
				if (created != null && created_range != null) {
					// TODO: input validation
					try {
						
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						Date result =  df.parse(created);
						Timestamp ts = new Timestamp(result.getTime());
						if (created_range.equals("before") && !item.getCreated().before(ts)) {
							continue;
						} else if (created_range.equals("after") && !item.getCreated().after(ts)) {
							continue;
						}
					} catch (Exception e) {}
				}
				results.add(item);
			}
		}
		if (rating != null && rating.length() > 0) {
			Collections.sort(results, new Comparator<RecipeDTO>() {

				@Override
				public int compare(RecipeDTO r0, RecipeDTO r1) {
					return -r0.getRating().compareTo(r1.getRating());
				}
				
			});
		}
		
		if (results.size() == 0) {
			alerts.add("NO_RESULTS");
		}
		
		return "success";
	}
	
	private boolean match(String match, Double data) {
		if (match == null || match.length() == 0) {
			return true;
		}
		if (data == null) {
			return false;
		}
		try {
			if (match.startsWith(">=")) {
				match = match.replaceAll("^>=\\s*", "");
				return data >= Integer.parseInt(match);
			} else if (match.startsWith("<=")){
				match = match.replaceAll("^<=\\s*", "");
				return data <= Integer.parseInt(match);
			} else if (match.startsWith(">")) {
				match = match.replaceAll("^>\\s*", "");
				return data > Integer.parseInt(match);
			} else if (match.startsWith("<")){
				match = match.replaceAll("^<\\s*", "");
				return data < Double.parseDouble(match);
			} else {
				return Math.round(data) == Math.round(Double.parseDouble(match));
			} 
		}
		catch (NumberFormatException e) {
			return true;
		}
		
	}

	public boolean match(String match, String data) {
		if (match == null || match.length() == 0) {
			return true;
		}
		data = data.toLowerCase();
		boolean isMatch = true;
		for (String outer : match.split("\\s+AND\\s+")) {
			boolean ormatch = false;
			for (String s : outer.split("\\s+OR\\s+")) {
				if (s.matches("^NOT\\s+.*$")) {
					s = s.replaceAll("^NOT\\s+", "");
					ormatch = ormatch || (!data.contains(s.toLowerCase()));					
				} else {
					ormatch = ormatch || data.contains(s.toLowerCase());
				}
			}
			isMatch = isMatch && ormatch;
		}

		return isMatch;
	}
	
	public boolean match(String match, List<IngredientDTO> ingredients) {
		if (match == null || match.length() == 0) {
			return true;
		}
		
		ArrayList<String> data = new ArrayList<String>();
		for (IngredientDTO i : ingredients) {
			data.add(i.getName().toLowerCase());
		}
		
		boolean isMatch = true;
		for (String outer : match.split("\\s+AND\\s+")) {
			boolean ormatch = false;
			for (String s : outer.split("\\s+OR\\s+")) {
				ormatch = ormatch || data.contains(s.toLowerCase());
			}
			isMatch = isMatch && ormatch;
		}
		return isMatch;
	}
	
	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public ArrayList<RecipeDTO> getResults() {
		return results;
	}

	public void setResults(ArrayList<RecipeDTO> results) {
		this.results = results;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getComplexity() {
		return complexity;
	}

	public void setComplexity(String complexity) {
		this.complexity = complexity;
	}

	public String getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	public String getServings() {
		return servings;
	}

	public void setServings(String servings) {
		this.servings = servings;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getCreated_range() {
		return created_range;
	}

	public void setCreated_range(String created_range) {
		this.created_range = created_range;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getRating() {
		return rating;
	}

}