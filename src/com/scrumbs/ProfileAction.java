package com.scrumbs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.scrumbs.data.RatedDAO;
import com.scrumbs.data.RatedDTO;
import com.scrumbs.data.RecipeDAO;
import com.scrumbs.data.RecipeDTO;
import com.scrumbs.data.SavedDAO;
import com.scrumbs.data.SessionDAO;
import com.scrumbs.data.UserDAO;
import com.scrumbs.data.UserDTO;
import com.scrumbs.data.WeeklyPlanDAO;
import com.scrumbs.data.WeeklyPlanDTO;

public class ProfileAction extends WebAction {
	public List<RecipeDTO> savedRecipes;
	public List<RecipeDTO> userRecipes;
	public List<WeeklyPlanDTO> userPlans;
	public Map<RecipeDTO, RatedDTO> ratedRecipes;
	public Integer user_id;
	public String username;
	public UserDTO userinfo;
	
	public String edit() {
		return "success";
	}
	
	public String execute() {
		try {
			UserDAO userDao = new UserDAO();
			SavedDAO savedDAO = new SavedDAO();
			RecipeDAO recipeDAO = new RecipeDAO();
			WeeklyPlanDAO planDAO = new WeeklyPlanDAO();
			RatedDAO ratedDAO = new RatedDAO();
			if (user_id != null) {
				userinfo = userDao.getUser(user_id);				
			} else if (username != null) {
				userinfo = userDao.getUser(username);
			} else if (loggedin) {
				userinfo = user;
			} else {
				return "error";
			}
			if (userinfo != null) {	
				List<Integer> savedRecipeList = savedDAO.getAllSaved(userinfo.getUser_id());
				savedRecipes = new ArrayList<RecipeDTO>();
				for (Integer z : savedRecipeList) {
					savedRecipes.add(recipeDAO.getRecipe(z));
				}
				ratedRecipes = ratedDAO.getRatedByUser(userinfo.getUser_id());
				userRecipes = recipeDAO.getRecipesByAuthor(userinfo.getUser_id());
				userPlans = planDAO.getPlans(userinfo.getUser_id());
			} else {
				return "error";
			}
		} catch (SQLException e) {
			// TODO
			e.printStackTrace();
		}
		return "success";
	}
}
