package com.scrumbs;

import java.sql.SQLException;

import com.scrumbs.data.RatedDAO;
import com.scrumbs.data.RecipeDAO;
import com.scrumbs.data.RecipeDTO;
import com.scrumbs.data.UserDTO;

public class RateAction extends WebAction {
	Integer recipeId;
	RecipeDTO recipe;
	Integer rating;
	
	public String execute() throws SQLException {
		RecipeDAO rdao = new RecipeDAO();
		if (loggedin) {	
			recipe = rdao.getRecipe(recipeId);
			RatedDAO ratedDAO = new RatedDAO();
			Integer currentRating = ratedDAO.getRated(user.getUser_id(), recipeId);
			if (currentRating == null) {
				ratedDAO.store(user.getUser_id(), recipeId, rating);
			} else {
				ratedDAO.update(user.getUser_id(), recipeId, rating);
			}
		}
		return "success";
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user =user;
	}

	public Integer getRecipeId() {
		return recipeId;
	}

	public void setRecipeId(Integer recipeId) {
		this.recipeId = recipeId;
	}
	public RecipeDTO getRecipe() {
		return recipe;
	}

	public void setRecipe(RecipeDTO recipe) {
		this.recipe = recipe;
	}
	
	public int getRating() {
		return rating;
	}
	
	public void setRating(int rating) {
		this.rating = rating;
	}

}
