package com.scrumbs;

import java.sql.SQLException;

import com.scrumbs.data.UnconfirmedDAO;
import com.scrumbs.data.UserDAO;

public class ConfirmAction extends WebAction {
	String activation_code;

	public String execute() throws SQLException {
		UserDAO userDao = new UserDAO();
		UnconfirmedDAO unconfirmedDao = new UnconfirmedDAO();
		
		if (activation_code != null && activation_code != "") {
			int user_id = unconfirmedDao.getActivatingUserid(activation_code);
			userDao.updateActivatedStatus(user_id);
			unconfirmedDao.cleanActivatedUser(activation_code);
			alerts.add("REGISTRATION_COMPLETE");
			
			return "success";
		}

		return "404";
	}

	public String getAsctivation_code() {
		return activation_code;
	}

	public void setActivation_code(String activation_code) {
		this.activation_code = activation_code;
	}
}
