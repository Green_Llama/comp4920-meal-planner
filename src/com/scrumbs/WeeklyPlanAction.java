package com.scrumbs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import org.apache.commons.lang3.text.WordUtils;

import com.scrumbs.data.IngredientDTO;
import com.scrumbs.data.RatedDAO;
import com.scrumbs.data.RecipeDAO;
import com.scrumbs.data.RecipeDTO;
import com.scrumbs.data.SavedDAO;
import com.scrumbs.data.WeeklyPlanDAO;
import com.scrumbs.data.WeeklyPlanDTO;

public class WeeklyPlanAction extends WebAction {
	public String clear;
	public Integer plan_id;
	public WeeklyPlanDTO plan;
	public List<WeeklyPlanDTO> userplans;
	public List<WeeklyPlanDTO> authorplans;
	
	public List<RecipeDTO> savedRecipes;
	public List<RecipeDTO> userRecipes;
	public List<RecipeDTO> ratedRecipes;
	
	Map<Integer, Integer> ratedMap;
	
	public SortedMap<String, List<String>> shoppingList;
	public String[] days = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
	public String[] mealtypes = {"Breakfast", "Lunch", "Dinner"};
	
	public String execute() throws SQLException {
		WeeklyPlanDAO wpdao = new WeeklyPlanDAO();
		RecipeDAO rdao = new RecipeDAO();
		SavedDAO sdao = new SavedDAO();
		RatedDAO ratedDAO = new RatedDAO();
		
		if (plan_id != null) {
			plan = wpdao.getPlan(plan_id);
			if (plan == null) {
				return "404";
			}
		} else {
			HashMap<String, HashMap<String, RecipeDTO>> mapping = new HashMap<String, HashMap<String, RecipeDTO>>();
			if (clear == null) {
				HashMap<String, List<RecipeDTO>> randomMeals = new HashMap<String, List<RecipeDTO>>();
				
				for (String mealtype : mealtypes) {
					List<RecipeDTO> meals = rdao.findRecipe(mealtype);
					if (!mealtype.equals("Breakfast")) {
						meals.addAll(rdao.findRecipe("meal"));
					}
					Collections.shuffle(meals);
					while (meals.size() < days.length) {
						meals.addAll(meals);
					}
					randomMeals.put(mealtype, meals);
				}
		
				for (String day : days) {
					HashMap<String, RecipeDTO> meals = new HashMap<String, RecipeDTO>();
					for (String mealtype : mealtypes) {
						meals.put(mealtype, randomMeals.get(mealtype).remove(0));
					}
					mapping.put(day, meals);
				}
			}
			plan = new WeeklyPlanDTO(0, null, null, null, mapping);
		}	
		generateShoppingList();
		
		if (loggedin) {
			userplans = wpdao.getPlans(user.getUser_id());
		} else {
			userplans = new ArrayList<WeeklyPlanDTO>();
		}
		
		if (plan_id != null && (!loggedin || plan.getUser().getUser_id() != user.getUser_id())) {
			authorplans = wpdao.getPlans(plan.getUser().getUser_id());
		} else {
			authorplans = new ArrayList<WeeklyPlanDTO>();
		}

		savedRecipes = new ArrayList<RecipeDTO>();
		if (loggedin && plan_id != null && plan.getUser().getUser_id() == user.getUser_id()) {
			ratedMap = ratedDAO.getRatingsByUserCheap(user.getUser_id());
			ratedRecipes = new ArrayList<RecipeDTO>();
			List<Integer> savedRecipeList = sdao.getAllSaved(user.getUser_id());
			for (Integer r_id : ratedMap.keySet()) {
				if (!savedRecipeList.contains(r_id)) {
					RecipeDTO ratedRecipe = rdao.getRecipe(r_id);
					if (ratedRecipe.getAuthor().getUser_id() != user.getUser_id()) {
						ratedRecipes.add(ratedRecipe);
					}
				}
			}
			Collections.sort(ratedRecipes, new Comparator<RecipeDTO>() {
				@Override
				public int compare(RecipeDTO r1, RecipeDTO r2) {
					return ratedMap.get(r1.getRecipe_id()).compareTo(ratedMap.get(r2.getRecipe_id()));
				}
			});
			userRecipes = rdao.getRecipesByAuthor(user.getUser_id());
			for (Integer z : savedRecipeList) {
				RecipeDTO savedRecipe = rdao.getRecipe(z);
				if (savedRecipe.getAuthor().getUser_id() != user.getUser_id()) {
					savedRecipes.add(rdao.getRecipe(z));
				}
			}
		} else {
			userRecipes = new ArrayList<RecipeDTO>();
			ratedRecipes = new ArrayList<RecipeDTO>();
		}
		
		return "success";
	}
	
	private void generateShoppingList() {
		shoppingList = new TreeMap<String, List<String>>();
		for (String day : plan.getEntries().keySet()) {
			for (RecipeDTO r : plan.getEntries().get(day).values()) {
				addIngredientToList(r, day);				
			}
		}
	}
	
	private void addIngredientToList(RecipeDTO r, String day) {
		for (IngredientDTO i : r.getIngredients()) {
			String name = WordUtils.capitalize(i.getName());
			if (!shoppingList.containsKey(name)) {
				shoppingList.put(name, new ArrayList<String>());
			}
			shoppingList.get(name).add(String.format("%s for %s on %s", i.getQuantity(), r.getName(), day));
		}
	}

	public Integer getPlan_id() {
		return plan_id;
	}

	public void setPlan_id(Integer plan_id) {
		this.plan_id = plan_id;
	}

	public String getClear() {
		return clear;
	}

	public void setClear(String clear) {
		this.clear = clear;
	}
}
