package com.scrumbs;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.scrumbs.data.PlanEntryDAO;
import com.scrumbs.data.PlanEntryDTO;
import com.scrumbs.data.RecipeDAO;
import com.scrumbs.data.RecipeDTO;
import com.scrumbs.data.WeeklyPlanDAO;
import com.scrumbs.data.WeeklyPlanDTO;

public class EditPlanAction extends WebAction {
	public Integer plan_id;
	public Integer recipe_id;
	public String recipe_name;
	public String day;
	public String meal;
	
	public Boolean delete;
	public Boolean update;
	public Boolean ajax;
	
	public RecipeDTO recipe;
	
	private List<String> days = Arrays.asList("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
	private List<String> mealtypes = Arrays.asList("Breakfast", "Lunch", "Dinner");
	
	public String execute() throws SQLException {
		if (loggedin && !(plan_id == null || day == null || meal == null)) {
			if (days.contains(day) && mealtypes.contains(meal)) {
				WeeklyPlanDAO wpdao = new WeeklyPlanDAO();
				PlanEntryDAO pedao = new PlanEntryDAO();
				WeeklyPlanDTO plan = wpdao.getPlan(plan_id);
				if (plan.user.getUser_id() != user.getUser_id()) {
					alerts.add("This isn't your meal plan.");
					return "failure";
				}
				
				if (update != null) {
					// Do updatey stuff.
					RecipeDAO rdao = new RecipeDAO();
					if (recipe_id != null) {
						recipe = rdao.getRecipe(recipe_id);
					} else if (recipe_name != null) {
						List<RecipeDTO> results = rdao.findRecipe(recipe_name);
						if (results.size() == 0) {
							recipe = null;
						} else {
							recipe = results.get(0);
						}
					} else {
						// Random recipe!
						List<RecipeDTO> results = rdao.findAll();
						if (results.size() == 0) {
							recipe = null;
						} else {
							Collections.shuffle(results);
							recipe = results.get(0);
						}
					}
					
					if (recipe == null) {
						alerts.add("That recipe doesn't exist.");
						return "failure";
					} else {
						pedao.store(new PlanEntryDTO(plan_id, recipe.getRecipe_id(), day, meal));
						if (ajax != null) {
							return "ajax";
						}
						return "success";
					}	
				} else if (delete != null) {
					// Do deletey stuff
					pedao.delete(plan_id, day, meal);
					if (ajax != null) {
						return "ajaxempty";
					}
					return "success";
				} else {
					return "404";
				}
			} else {
				alerts.add("Invalid day or meal.");
				return "failure";
			}
		}
		return "404";
	}


	public Integer getPlan_id() {
		return plan_id;
	}


	public void setPlan_id(Integer plan_id) {
		this.plan_id = plan_id;
	}


	public String getDay() {
		return day;
	}


	public void setDay(String day) {
		this.day = day;
	}


	public String getMeal() {
		return meal;
	}


	public void setMeal(String meal) {
		this.meal = meal;
	}


	public Integer getRecipe_id() {
		return recipe_id;
	}


	public void setRecipe_id(Integer recipe_id) {
		this.recipe_id = recipe_id;
	}


	public String getRecipe_name() {
		return recipe_name;
	}


	public void setRecipe_name(String recipe_name) {
		this.recipe_name = recipe_name;
	}


	public Boolean getDelete() {
		return delete;
	}


	public void setDelete(Boolean delete) {
		this.delete = delete;
	}


	public Boolean getUpdate() {
		return update;
	}


	public void setUpdate(Boolean update) {
		this.update = update;
	}


	public Boolean getAjax() {
		return ajax;
	}


	public void setAjax(Boolean ajax) {
		this.ajax = ajax;
	}
}
