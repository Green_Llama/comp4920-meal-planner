package com.scrumbs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang3.text.WordUtils;

import com.scrumbs.data.IngredientDTO;
import com.scrumbs.data.RecipeDTO;
import com.scrumbs.data.WeeklyPlanDAO;
import com.scrumbs.data.WeeklyPlanDTO;

public class ShoppingListAction extends WebAction {
	public Integer plan_id;
	public String ajax;
	
	public WeeklyPlanDTO plan;
	public SortedMap<String, List<String>> shoppingList;

	public String execute() throws SQLException {
		WeeklyPlanDAO wpdao = new WeeklyPlanDAO();
		if (plan_id != null) {
			plan = wpdao.getPlan(plan_id);
			if (plan == null) {
				return "404";
			}
		} else {
			return "404";
		}
		generateShoppingList();
		if (ajax != null) {
			return "ajax";
		}
		return "success";
	}
	private void generateShoppingList() {
		shoppingList = new TreeMap<String, List<String>>();
		for (String day : plan.getEntries().keySet()) {
			for (RecipeDTO r : plan.getEntries().get(day).values()) {
				addIngredientToList(r, day);				
			}
		}
	}
	
	private void addIngredientToList(RecipeDTO r, String day) {
		for (IngredientDTO i : r.getIngredients()) {
			String name = WordUtils.capitalize(i.getName());
			if (!shoppingList.containsKey(name)) {
				shoppingList.put(name, new ArrayList<String>());
			}
			shoppingList.get(name).add(String.format("%s for %s on %s", i.getQuantity(), r.getName(), day));
		}
	}
	public Integer getPlan_id() {
		return plan_id;
	}
	public void setPlan_id(Integer plan_id) {
		this.plan_id = plan_id;
	}
	public String getAjax() {
		return ajax;
	}
	public void setAjax(String ajax) {
		this.ajax = ajax;
	}
}
