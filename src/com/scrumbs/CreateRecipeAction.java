package com.scrumbs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.scrumbs.data.IngredientDAO;
import com.scrumbs.data.IngredientDTO;
import com.scrumbs.data.RecipeDAO;
import com.scrumbs.data.RecipeDTO;
import com.scrumbs.data.UserDAO;
import com.scrumbs.data.UserDTO;

public class CreateRecipeAction extends WebAction {
	public RecipeDTO recipe;
	
	// Recipe fields
	public Integer recipe_id;
	public String name; 
	public String description;
	public String picture;
	public String method; 
	public UserDTO author;
	public String servings;
	public String category; 
	public String complexity;
	public Integer parent;
	public String ingredients_data;
	
	public File picture_file;
		
	public String ajax;
	public String update;
	public String remix;
	public String delete;
	
	public String execute() {
		if (!loggedin) {
			return "404";
		}
		return "success";
	}

	public String save() throws SQLException {
		RecipeDAO storer = new RecipeDAO();
		IngredientDAO istorer = new IngredientDAO();
		
		List<IngredientDTO> ingredients = new ArrayList<IngredientDTO>();

		if (!loggedin) {
			return "404";
		}
		
		if (delete != null) {
			// All we need to validate is recipe junk.
			if (parent != null) {
				recipe = storer.getRecipe(parent);
				if (recipe != null && recipe.getAuthor().getUser_id() == user.getUser_id()) {
					storer.delete(parent);
					return "delete";
				}
			}
			return "error";
		} else {
			// validate the recipe
			validate();
			if (parent != null && storer.getRecipe(parent) == null) {
				alerts.add("Invalid parent recipe.");
			}
			if (!alerts.isEmpty()) {
				if (ajax != null) {
					return "ajaxempty";
				}
				return "error";
			}
	
			Pattern re = Pattern.compile("^(.+?)\\s*\\((.+)\\)$");
			for (String i : ingredients_data.split("\n")) {
				Matcher m = re.matcher(i);
				if (!m.find()) {
					alerts.add("Ingredients must match the format `Ingredient (Quantity)`, one per line.");
					if (ajax != null) {
						return "ajaxempty";
					}
					return "error";
				} else {
					IngredientDTO ingredient = new IngredientDTO();
					ingredient.setName(m.group(1));
					ingredient.setQuantity(m.group(2));
					ingredients.add(ingredient);
				}
			}

			// Three operations left: Update, remix or create.
			if (update != null) {
				recipe = storer.update(parent, user.getUser_id(), name, description, picture, method, servings, category, complexity);
				// This will only update if the user_id column matches. So we can validate /after/ the call (cheaper)
				if (recipe == null) {
					alerts.add("Recipe doesn't exist.");
					return "error";
				} else if (recipe.getAuthor().getUser_id() != user.getUser_id()) {
					alerts.add("You don't own that recipe.");
					return "error";
				}
				istorer.deleteAll(recipe.getRecipe_id());
				for (IngredientDTO i : ingredients) {
					i.setRecipe(recipe.getRecipe_id());
					istorer.store(i);
				}
				System.out.println(picture_file);
				// Save the new picture if one is provided.
				if (picture_file != null) {
					try {
						FileUtils.copyFile(picture_file, getImageFile(recipe.getRecipe_id()));
						storer.setPicture(recipe.getRecipe_id(), "images/recipe_" + recipe.getRecipe_id().toString() + ".png");
					} catch (IOException e) {
						// TODO
						e.printStackTrace();
					}
				}
				// If picture is empty, set it to the uploaded default
				if (picture == null || picture.length() == 0) {
					storer.setPicture(recipe.getRecipe_id(), "images/recipe_" + recipe.getRecipe_id().toString() + ".png");
				}
				return "success";
			} else {
				// Phew! The last two cases are actually exactly the same case. Almost.
				if (remix != null) {
					// If it's a remix, we just need to check the parent recipe exists.
					if (parent == null) {
						alerts.add("Sorry. Something went wrong.");
						return "error";
					}
					RecipeDTO parent_obj = storer.getRecipe(parent);
					if (parent_obj == null) {
						// But if it doesn't, we just ignore it.
						parent = null;
					}
				}
				// Now they're the same!
				
				Date currentTime = new Date();
				Timestamp created = new Timestamp(currentTime.getTime());
				
				recipe = storer.store(name, description, picture, method, user.getUser_id(), created, servings, category, complexity, parent);
					
				for (IngredientDTO i : ingredients) {
					i.setRecipe(recipe.getRecipe_id());
					istorer.store(i);
				}
				
				if (remix != null  && (picture == null || picture.length() == 0)) {
					// I lied. We might need to copy over the picture.
					File parent_file = getImageFile(parent);
					if (parent_file.exists()) {
						try {
							FileUtils.copyFile(parent_file, getImageFile(recipe.getRecipe_id()));
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				
				// Save the new picture if one is provided.
				if (picture_file != null) {
					try {
						FileUtils.copyFile(picture_file, getImageFile(recipe.getRecipe_id()));
					} catch (IOException e) {
						// TODO
						e.printStackTrace();
					}
				}
				if (picture == null || picture.length() == 0) {
					storer.setPicture(recipe.getRecipe_id(), "images/recipe_" + recipe.getRecipe_id().toString() + ".png");
				}
				
				if (ajax != null) {
					return "ajax";
				}
			}
		}
		return "success";
	}
	
	private File getImageFile(Integer recipe_id) {
		return new File(request.getSession().getServletContext().getRealPath("/"), "images/recipe_" + recipe_id.toString() + ".png");
	}
	
	public void validate() {
		// TODO: check uniqueness
		// Validation
		if (name == null || (picture == null && picture_file == null && update == null && remix == null) || description == null || method == null || servings == null || category == null || complexity == null) {
			alerts.add("Missing required field.");
			return;
		}
		if (name.length() > 255 || name.length() < 1) {
			alerts.add("Invalid recipe name. Must be less than 255 characters and greater than 0 characters.");
		}
		if (picture != null && picture.length() > 255)
			alerts.add("URL is too long. Must be <255 characters.");
		if (description.length() > 4096)
			alerts.add("Invalid description. Must be less than 4096 characters.");
		if (method.length() > 65535 || method.length() < 1)
			alerts.add("Invalid method. Must be less than 65535 characters and greater than 0 characters.");
		if (servings.length() > 255 || servings.length() < 1) {
			alerts.add("Invalid number of servings. Must be less than 255 characters and greater than 0 characters.");
		}
		if (category.length() > 255 || category.length() < 1) {
			alerts.add("Invalid category. Must be less than 255 characters and greater than 0 characters.");
		}
		if (!(complexity.equals("Easy") || complexity.equals("Hard") || complexity.equals("Medium"))) {
			alerts.add("Invalid complexity. Choose between Easy, Medium or Hard.");
		}
	}
	
	/*------------------------Getter and Setters----------------------*/
	
	public RecipeDTO getRecipe() {
		return recipe;
	}

	public void setRecipe(RecipeDTO newRecipe) {
		this.recipe = newRecipe;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public UserDTO getAuthor() {
		return author;
	}

	public void setAuthor(UserDTO author) {
		this.author = author;
	}

	public String getServings() {
		return servings;
	}

	public void setServings(String servings) {
		this.servings = servings;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getComplexity() {
		return complexity;
	}

	public void setComplexity(String complexity) {
		this.complexity = complexity;
	}

	public int getParent() {
		return parent;
	}

	public void setParent(int parent) {
		this.parent = parent;
	}

	public String getIngredients_data() {
		return ingredients_data;
	}

	public void setIngredients_data(String ingredients_data) {
		this.ingredients_data = ingredients_data;
	}

	public String getAjax() {
		return ajax;
	}

	public void setAjax(String ajax) {
		this.ajax = ajax;
	}

	public Integer getRecipe_id() {
		return recipe_id;
	}

	public void setRecipe_id(Integer recipe_id) {
		this.recipe_id = recipe_id;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public String getRemix() {
		return remix;
	}

	public void setRemix(String remix) {
		this.remix = remix;
	}

	public String getDelete() {
		return delete;
	}

	public void setDelete(String delete) {
		this.delete = delete;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

	public File getPicture_file() {
		return picture_file;
	}

	public void setPicture_file(File picture_file) {
		this.picture_file = picture_file;
	}

}
