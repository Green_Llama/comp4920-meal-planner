<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>${recipe.name} - Scrumbs</title>
<%@include file="templates/headers.jsp" %>
</head>
<body>
	<div id="wrap">
		<!-- Static navbar -->
		<%@include file="templates/navbar.jsp" %>
		<br />
		<%@include file="templates/remixModal.jsp" %>
		<div class="container">
			<%@include file="templates/alerts.jsp" %>
			<div class="well">
					<img src="${recipe.picture}" class="pull-right img-thumbnail" style="height: 200px;"
					alt="" />
				<div style="min-height: 200px;">
					<form action="favorite" method="POST">
						<div class="form-inline">

						<input type="hidden" name="recipeId" value="${recipe.recipe_id}" />
						<h2 style="margin-bottom:2px;">${recipe.name}&nbsp;&nbsp;

						<s:if test="loggedin==true">
							<s:if test="loggedin && recipe.author.user_id == user.user_id">
								<button type="button" class="btn btn-primary" data-toggle="modal" href="#remixModal">Modify</button>
							</s:if>
							<s:else>
								<button type="button" class="btn btn-primary" data-toggle="modal" href="#remixModal">Remix</button>
							</s:else>
							<c:choose>
							      <c:when test="${saved==false}">
							      	<button type="submit" class="btn btn-primary">Favourite</button>
							      </c:when>
							
							      <c:otherwise> 
							      	<button type="submit" class="btn btn-default">Unfavourite</button>
							  
							      </c:otherwise>
							</c:choose>
							<div class="form-group" style="width:115px;">
							<div class="input-group">
      						<span class="input-group-btn"> 
								<button type="submit" class="btn btn-primary" form="rating">Rate</button>
					         </span>
						
							<select name="rating" form="rating" class="form-control" style="width: 70px">
								<!-- Save a bit of code. -->
								<s:iterator var="i" begin="0" end="5">
								<c:choose>
									<c:when test="${i == rating}">
										<option value="${i}" selected="selected">${i}</option>
									</c:when>
									<c:otherwise>
										<option value="${i}">${i}</option>
									</c:otherwise>
								</c:choose>
								</s:iterator>
							</select>
							</div>
							</div>
							<c:if test="${user.getRatings().containsKey(recipe.recipe_id)}">
							<div class="pull-right">
								<c:set var="givenRating" value="${user.getRatings().get(recipe.recipe_id)}" />
								<small><%@include file="templates/displayrating.jsp" %></small>
								&nbsp;
							</div>
							</c:if>				
						</s:if>
						</h2>
						<s:if test="parent != null">
							<em><small>Remixed from 
								<s:a action="recipe">
									<s:param name="recipeId">${parent.recipe_id}</s:param>
									${parent.name}
								</s:a>
							</small></em>
							<br />
						</s:if>
						</div>
					</form>
					<form action="rate" method="POST" id="rating">
						<input type="hidden" name="recipeId" value="${recipe.recipe_id}" />
					</form>
					<br />
					${recipe.description} 
				</div>
				<br />
				<div class="panel panel-primary">
					<div class="panel-heading" style="position: relative;">
						${recipe.name}
						<form action="results" method="GET">
						<input type="hidden" name="category" value="${recipe.category}" />
						<button class="btn" type="submit"
							style="border-radius: 0px 3px; right: 0px; top: 0px; height: 100%; position: absolute; padding-left: 30px; padding-right: 30px;">${recipe.category}</button>
						</form>
					</div>
					<div class="panel-body">
						<div class="well" style="float: right">
							<!--  Picture goes here or something. -->
							<h4>Ingredients</h4>
							<ul class="list-group">
								<s:iterator value="recipe.ingredients" var="ingredient">
								<s:a cssClass="list-group-item" action="results">
									<s:param name="ingredients">${ingredient.name}</s:param>
									<p class="list-group-item-text">${ingredient.name}&nbsp;&nbsp;&nbsp;&nbsp;<span style="float:right;"><small>${ingredient.quantity}</small></span></p>
								</s:a>
								</s:iterator>
							</ul>
						</div>
						<div>
								${recipe.htmlMethod()}
						</div>
						<br />
						<br />
						<span class="label label-primary">${recipe.complexity}</span>
						<s:if test="recipe.rating != null">
							<span class="label label-warning">Rated ${recipe.ratingString}</span>
						</s:if>
						
					</div>
					<div class="panel-footer" style="position: relative;">
						Serves ${recipe.servings}
						<div style="position: absolute; right: 10px; top: 10px;">by
							<s:a action="profile">
							<s:param name="username">${recipe.author.username}</s:param>
							${recipe.author.username}</s:a>
						</div>
					</div>	
				</div>
			</div>
			<!-- Comments and other metadata etc go here. -->
			<div class="row">
			<div class="col-md-8">
			<s:if test="loggedin || comments.size() > 0">
				<h4>Comments</h4>
			</s:if>
			<div class="list-group">
				<c:forEach var="comment" items="${comments}" >
					<div class="list-group-item">
					<p class="list-group-item-text">
						<s:a action="profile">
							<s:param name="username">${comment.user.username}</s:param>
							<b>${comment.user.firstname} ${comment.user.lastname}</b>
						</s:a>
						&nbsp;
						${comment.text}
						<c:if test="${comment.user.getRatings().containsKey(recipe.recipe_id)}">
							<div class="pull-right">
								<c:set var="givenRating" value="${comment.user.getRatings().get(recipe.recipe_id)}"/>
								<%@include file="templates/displayrating.jsp" %>
							</div>
						</c:if>
					</p>
				</div>
				</c:forEach>
			
			<s:if test="loggedin == true">
				<form action="recipe" method="POST">
					<div class="list-group-item">
							<div class="input-group">
							<s:a action="profile" cssClass="input-group-addon">
								<s:param name="username">${user.username}</s:param>
								<b>${user.firstname} ${user.lastname}</b>
							</s:a>
								<input type="hidden" name="recipeId" value="${recipe.recipe_id}" />
								<input name="postComment" placeholder="My tongue is in heaven." class="form-control" />
								<span class="input-group-btn">
									<button type="submit" class="btn btn-default">Comment</button>
								</span>
							</div>
					</div>
				</form>
			</s:if>
			</div>
			</div>
			<div class="col-md-4">
			<s:if test="children.size() > 0">
				<h4>Remixes</h4>
				<c:forEach var="recipe" items="${children}" >
					<%@include file="templates/recipeentry-minimal.jsp"%>
				</c:forEach>
			</s:if>
			<s:if test="similar.size() > 0">
				<h4>Similar recipes</h4>
				<c:forEach var="recipe" items="${similar}" >
					<%@include file="templates/recipeentry-minimal.jsp"%>
				</c:forEach>
			</s:if>
			</div>
		</div>
	</div>
	<%@include file="templates/footer.jsp" %>
	<%@include file="templates/scripts.jsp" %>
</body>
</html>