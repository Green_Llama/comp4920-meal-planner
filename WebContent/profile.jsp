<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Profile Page</title>
<%@include file="templates/headers.jsp"%>
</head>
<body>
	<div id="wrap">
		<!-- Static navbar -->
		<%@include file="templates/navbar.jsp"%>
		<s:if test="loggedin && userinfo.user_id == user.user_id">
		<!-- Edit Profile Modal -->
		<div class="modal fade" id="editProfile" tabindex="-1" role="dialog"
			aria-labelledby="editProfileModal" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Edit your profile</h4>
					</div>
					<div class="modal-body">
					<form action="editProfile" method="POST" class="form-horizontal" id="editProfileForm"
						role="form">
						<div class="form-group">
							<label for="username_field" class="col-lg-4 control-label">Username</label>
							<div class="col-lg-8">
								<input type="text" name="new_username" maxlength="32"
									id="username_field" value="${user.username}"
									placeholder="If I can't pronounce it, it's a bad username."
									class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label for="old_password_field" class="col-lg-4 control-label">Old
								password</label>
							<div class="col-lg-8">
								<input type="password" name="o_password" maxlength="64"
									id="old_password_field"
									placeholder="Mandatory. Just to make sure it's you."
									class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label for="password_field" class="col-lg-4 control-label">New
								password</label>
							<div class="col-lg-8">
								<input type="password" name="password" maxlength="64"
									id="password_field" placeholder="hunter2" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label for="confirm_password_field"
								class="col-lg-4 control-label">Confirm password</label>
							<div class="col-lg-8">
								<input type="password" name="c_password" maxlength="64"
									id="confirm_password_field" placeholder="And again."
									class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label for="firstname_field" class="col-lg-4 control-label">First
								name</label>
							<div class="col-lg-8">
								<input type="text" name="firstname" maxlength="255"
									id="firstname_field" value="${user.firstname}"
									placeholder="You mean you're name isn't really ${user.firstname}?"
									class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label for="lastname_field" class="col-lg-4 control-label">Last
								name</label>
							<div class="col-lg-8">
								<input type="text" name="lastname" maxlength="255"
									id="lastname_field" value="${user.lastname}"
									placeholder="You don't have to tell us. It's okay."
									class="form-control" />
							</div>
						</div>
					</form>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary" form="editProfileForm">Submit</button>
					</div>
					</div>
				</div>

				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		</s:if>
		<div class="container">
			<div class="page-header">
			<%@include file="templates/alerts.jsp" %>
				<s:if test="loggedin && userinfo.user_id == user.user_id">
					<div class="pull-right">
						<a data-toggle="modal" href="#editProfile" class="btn btn-primary">Edit Profile</a>
					</div>
				</s:if>
				<h2>${userinfo.username}'s profile</h2>
				<!-- add friend etc goes here -->
			</div>
			<div class="row">
				<div class="col-md-6">
					<h3>Name</h3>
					<h4>
						<b>${userinfo.firstname}</b> ${userinfo.lastname}
					</h4>
				</div>
				<s:if test="loggedin && userinfo.user_id == user.user_id">
				<div class="col-md-6">
					<h3>E-mail</h3>
					<h4>
						<b>${userinfo.email}</b>
					</h4>
				</div>
				</s:if>
			</div>
			<s:if test="userRecipes.size() > 0">
				<br />
				<h3>${userinfo.username}'s recipes</h3>
				<div class="list-group">
					<c:forEach var="recipe" items="${userRecipes}">
						<%@include file="templates/recipeentry.jsp"%>
					</c:forEach>
				</div>
			</s:if>
			<s:if test="savedRecipes.size() > 0">
				<br />
				<h3>${userinfo.username}'s favourites</h3>
				<div class="list-group">
					<c:forEach var="recipe" items="${savedRecipes}">
						<%@include file="templates/recipeentry.jsp"%>
					</c:forEach>
				</div>
			</s:if>
			<s:if test="ratedRecipes.size() > 0">
				<br />
				<h3>${userinfo.username}'s ratings</h3>
				<div class="list-group">
					<c:forEach var="entry" items="${ratedRecipes}">
						<c:set var="recipe" value="${entry.key}" />
						<c:set var="personalRating" value="${entry.value }" />
						<%@include file="templates/recipeentry.jsp"%>
					</c:forEach>
				</div>
			</s:if>
			<s:if test="userPlans.size() > 0">
				<br />
				<h3>${userinfo.username}'s meal plans</h3>
				<div class="list-group">
					<c:forEach var="p" items="${userPlans}">
						<s:a action="weeklyPlan" cssClass="list-group-item">
							<s:param name="plan_id">${p.plan_id}</s:param>
							${p.name}
						</s:a>
					</c:forEach>
				</div>
			</s:if>
		</div>
		<%@include file="templates/footer.jsp"%>
		<%@include file="templates/scripts.jsp"%>
	</div>
</body>
</html>