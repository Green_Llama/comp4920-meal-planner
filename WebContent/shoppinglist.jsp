<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Generate a Weekly Plan - Scrumbs</title>
<%@include file="templates/headers.jsp" %>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3>Shopping List</h3>
				<ul class="list-group">
					<s:iterator value="shoppingList">
						<div class="list-group-item">
							${key}
							<div style="align:right;">
								<s:iterator value="value" var="k">
									<i><small>${k}</small></i> <br />
								</s:iterator>
							</div>
						</div>
					</s:iterator>
				</ul>
			</div>
		</div>
	</div>
	<%@include file="templates/scripts.jsp" %>
</body>
</html>