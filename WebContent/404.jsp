<!DOCTYPE html>
<html lang="en-US" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- 404 page taken from Joey Tuong's COMP2041 Assignment 2 -->
<title>That page doesn't exist. Please leave. - Scrumbs</title>
<!-- Google hosted Londrina Sketch font -->
<link href='http://fonts.googleapis.com/css?family=Londrina+Sketch'
	rel='stylesheet' type='text/css'>
<!-- Google hosted Open Sans -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css'>
<!-- Javascript adapted from w3schools clock example -->
<script type="text/javascript">
            var colours=[Math.floor(Math.random()*256),Math.floor(Math.random()*256),Math.floor(Math.random()*256)];
            var darker = -1;
			function setHome() {
	            document.getElementById("home").style.color = "rgba("+colours[0]+", "+colours[1]+", "+colours[2]+", 0.75)";
			}
            function changeColour() {
                var index = Math.floor(Math.random()*3);
                var darkness = (colours[0] + colours[1] + colours[2]) / 3;
                if (darkness < 40) {
                    darker = 1;
                } else if (darkness > 210){
                    darker = -1;
                }
                
                if (colours[index] == 255) {
                    colours[index] = 247;
                } else if (colours[index] == 0) {
                    colours[index] = 8;
                } else {
                    colours[Math.floor(Math.random()*3)] += Math.ceil(Math.random()*4) * darker;
                }
                document.body.style.backgroundColor = "rgba("+colours[0]+", "+colours[1]+", "+colours[2]+", 0.75)";
                // document.getElementById("home").style.color = "rgba("+(256- colours[0])+", "+(256 - colours[1])+", "+(256 - colours[2])+", 0.75)";
                t=setTimeout(function(){changeColour(); },100);
            }
            function glitch() {

            	if (Math.floor(Math.random() * 40) == 0) {
            		var glitchelement = document.getElementsByClassName("glitchy")[Math.floor(Math.random() * 7)];
            		var coeff = (Math.random() * 0.5);
            		var cdiff = [coeff * (256-colours[0]), coeff * (256-colours[1]), coeff*(256-colours[2])];
                    var c = [Math.floor(cdiff[0] + colours[0]), Math.floor(cdiff[1] + colours[1]), Math.floor(cdiff[2] + colours[2])];
                    document.body.style.backgroundColor = "rgba("+c[0]+", "+c[1]+", "+c[2]+", 0.75)";
            		glitchelement.style.fontFamily = "'Londrina Sketch',bold";
            		t=setTimeout(function() { glitchelement.style.fontFamily = "'Open Sans'"; }, 60);
            	}
            	
            	t=setTimeout(function(){glitch(); }, 555);
            }

        </script>
<style type="text/css">
.glitchy {
	font-family: 'Open Sans';
	width: 100%;
	text-align: center;
	position: absolute;
}
</style>
</head>

<body onload="setHome(); changeColour(); glitch();" style="transform:rotate(-0.5deg);">
	<div
		style="font-family: 'Londrina Sketch', bold; text-align: center; font-size: 314px; height: 100%;">404</div>
	<div class="glitchy" style="font-size: 75px; top: 350px;">this
		page doesn't exist</div>
	<div class="glitchy" style="font-size: 60px; top: 440px;">it
		never existed</div>
	<div class="glitchy" style="font-size: 25px; top: 520px;">this
		page is in your head</div>
	<div class="glitchy" style="font-size: 16px; top: 555px;">it
		always was</div>
	<div class="glitchy" style="font-size: 9px; top: 580px;">honest</div>
	<div class="glitchy" style="font-size: 5px; top: 590px;">.</div>
	<a class="glitchy" id="home"
		style="font-size: 40px; top: 650px; text-decoration: none;" href="/">
		please take me home </a>

</body>
</html>