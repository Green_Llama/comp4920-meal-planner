<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<input type="hidden" name="plan_id" value="${plan_id}" />
<input type="hidden" name="meal" value="${meal}" />
<input type="hidden" name="day" value="${day}" />
<div class="droppable">
<div class="planentry-empty-options">
<br/>
	<button data-toggle="tooltip" class="replace-popover" type="button" title="Add"
						data-content='<input type="hidden" name="update" form="form-${day}-${meal}"/><input type="text" placeholder="Noodles?" name="recipe_name" form="form-${day}-${meal}" class="form-control" style="width:200px"/>'
						>
		<span class="glyphicon glyphicon-plus"></span>
	</button>
	&nbsp;
	<button data-toggle="tooltip" title="Randomize" name="update">
 		<span class="glyphicon glyphicon-refresh"></span>
 	</button>
</div>
</div>