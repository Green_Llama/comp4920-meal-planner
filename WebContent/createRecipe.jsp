<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Create a Recipe - Scrumbs</title>
<%@include file="templates/headers.jsp" %>
</head>
<body>
	<div id="wrap">
		<!-- Static navbar -->
		<%@include file="templates/navbar.jsp" %>
		<br />
		<div class="container">
			<%@include file="templates/alerts.jsp" %>
			<form action="saveRecipe" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
			<div class="create-recipe-form">
				<div class="form-group">
					<label for="inputname" class="col-lg-2 control-label">Name</label>
					<div class="col-lg-10"><input id="inputname" type="text" class="form-control" placeholder="The title of your recipe" name="name">
					</div>
				</div>
				<div class="form-group">
					<label for="inputdesc" class="col-lg-2 control-label">Description</label>
					<div class="col-lg-10"><input id="inputdesc" type="text" class="form-control" placeholder="A short description" name="description">
				</div></div>
				<div class="form-group">
					<label for="inputpic" class="col-lg-2 control-label">Picture</label>
					<div class="col-lg-10">
						<a class="btn btn-default pull-right" style="margin-left:5px;" id="clearbutton">Clear</a>
						<div class="input-group">
							<input id="inputpic" type="text" class="form-control" placeholder="A direct link to an image of your recipe" name="picture">
							<span class="input-group-btn">
								<a class="btn btn-primary" onclick="$('input[id=inputpicfile]').click();">Upload</a>
							</span>
						</div>
					</div>
				</div>
				<input id="inputpicfile" name="picture_file" type="file" style="display:none" />
				<div class="form-group">
					<label for="inputingr" class="col-lg-2 control-label">Ingredients</label>
					<div class="col-lg-10"><textarea id="inputingr" type="text" class="form-control" name="ingredients_data" placeholder="Milk (2 cups)"></textarea>
								</div></div>
				<div class="form-group">
					<label for="inputmeth" class="col-lg-2 control-label">Method</label>
					<div class="col-lg-10"><textarea id="inputmeth" type="text" class="form-control" placeholder="Tell us how to make it!" name="method"></textarea>
				</div></div>
				<div class="form-group">
					<label for="inputserv" class="col-lg-2 control-label">Servings</label>
					<div class="col-lg-10"><input id="inputserv" type="text" class="form-control" placeholder="How many people does it serve?" name="servings">
				</div></div>
				<div class="form-group">
					<label for="inputcat" class="col-lg-2 control-label">Category</label>
					<!-- TODO: Turn this into a dropdown? -->
					<div class="col-lg-10"><input id="inputcat" type="text" class="form-control" placeholder="What category does this fit under? Breakfast, Dessert, Beverage...?" name="category">
				</div></div>
				<div class="form-group">
					<label for="inputcomp" class="col-lg-2 control-label">Complexity</label>
					<div class="col-lg-10">
					<select id="inputcomp" name="complexity" class="form-control">
						<option value="Easy">Easy</option>
						<option value="Medium">Medium</option>
						<option value="Hard">Hard</option>
					</select>
				</div></div>
				<br/>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Create!</button>
				</div>
				</div>
			</form>


		</div>
	</div>
	<script type="text/javascript">
	function resetFormElement(e) {
		// http://stackoverflow.com/questions/1043957/clearing-input-type-file-using-jquery
		e.wrap('<form>').closest('form').get(0).reset();
		e.unwrap();
	};
    $(document).ready(function () {
		$('input[id=inputpicfile]').change(function() {
			$('#inputpic').val($(this).val());
			$("#inputpic").prop("disabled", true);
		});
		var control = $("#inputpicfile");
		$("#clearbutton").on("click", function () {
		    resetFormElement($("#inputpicfile"));
			$("#inputpic").prop("disabled", false);
		});
    });
	</script>
	<%@include file="templates/footer.jsp" %>
	<%@include file="templates/scripts.jsp" %>
</body>
</html>