<c:forEach var="stars" begin="0" end="4" >
	<c:choose>
		<c:when test="${givenRating > stars}">
		<span class="glyphicon glyphicon-star"></span>
		</c:when>
		<c:otherwise>
		<span class="glyphicon glyphicon-star-empty"></span>
		</c:otherwise>
	</c:choose>

</c:forEach>