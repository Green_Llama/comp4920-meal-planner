<%@ taglib prefix="s" uri="/struts-tags"%>

<s:a action="recipe" cssClass="list-group-item recipe-entry" data-recipe="${recipe.recipe_id}" cssStyle="min-height:60px;">
	<s:param name="recipeId">${recipe.recipe_id}</s:param>
	<div class="pull-right" style="min-width:70px;text-align:center;">
		<div class="img-thumbnail" style="width:100%;margin-left:5px;">
			<img src="${recipe.picture}" style="height:35px;"/>
		</div>
	</div>
	${recipe.name}
	<c:if test="${loggedin && user.getRatings().containsKey(recipe.recipe_id)}">
		<div class="pull-right">
		<c:set var="givenRating" value="${user.getRatings().get(recipe.recipe_id)}" />
		<small><%@include file="displayrating.jsp" %></small>
		&nbsp;
		</div>
	</c:if>
</s:a>