<s:a action="recipe" cssClass="list-group-item">
	<s:param name="recipeId">${recipe.recipe_id}</s:param>
	<div class="pull-right" style="min-width:70px;text-align:center;">
		<div class="img-thumbnail" style="width:100%;margin-left:5px;">
			<img src="${recipe.picture}" style="height:35px;"/>
		</div>
	</div>
	<c:if test="${loggedin && user.getRatings().containsKey(recipe.recipe_id)}">
		<div class="pull-right">
		<c:set var="givenRating" value="${user.getRatings().get(recipe.recipe_id)}" />
		<small><%@include file="displayrating.jsp" %></small>
		&nbsp;
		</div>
	</c:if>
	<h4 class="list-group-item-heading">${recipe.name}</h4>		
	<div class="pull-right">
		<span class="label label-default">${recipe.category}</span>
		<span class="label label-primary">${recipe.complexity}</span>
		<c:choose>
			<c:when test="${recipe.rating == null}">
				<span class="label label-warning">Unrated</span>
			</c:when>
			<c:otherwise>
				<span class="label label-warning">Rated ${recipe.ratingString}</span>
			</c:otherwise>
		</c:choose>
	</div>				
	<p class="list-group-item-text"><i>submitted by ${recipe.author.username}</i></p>
</s:a>