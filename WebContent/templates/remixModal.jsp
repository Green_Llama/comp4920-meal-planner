<!-- Remix Modal -->
<div class="modal fade" id="remixModal" tabindex="-1" role="dialog"
	aria-labelledby="remixModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="saveRecipe" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="parent" value="${recipe.recipe_id}" />
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<s:if test="loggedin && recipe.author.user_id == user.user_id">
						<h4 class="modal-title">Edit or remix this recipe</h4>
				</s:if>
				<s:else>
						<h4 class="modal-title">Remix this recipe</h4>
				</s:else>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
				<div class="form-group">
					<label for="inputname" class="col-lg-2 control-label">Name</label>
					<div class="col-lg-10"><input id="inputname" type="text" class="form-control" placeholder="The title of your recipe" name="name" value="${recipe.name}">
					</div>
				</div>
				<div class="form-group">
					<label for="inputdesc" class="col-lg-2 control-label">Description</label>
					<div class="col-lg-10"><input id="inputdesc" type="text" class="form-control" placeholder="A short description" name="description" value="${recipe.description}">
				</div></div>
				<div class="form-group">
					<label for="inputpic" class="col-lg-2 control-label">Picture</label>
					<div class="col-lg-10">
						<a class="btn btn-default pull-right" style="margin-left:5px;" id="clearbutton">Clear</a>
						<div class="input-group">
							<c:choose>
							<c:when test='${recipe.picture.startsWith("images/")}'>
							<input id="inputpic" type="text" class="form-control" placeholder="A direct link to an image of your recipe" name="picture" disabled>
							</c:when>
							<c:otherwise>
							<input id="inputpic" type="text" class="form-control" placeholder="A direct link to an image of your recipe" name="picture" value="${recipe.picture}">
							</c:otherwise>
							</c:choose>
							<span class="input-group-btn">
								<a class="btn btn-primary" onclick="$('input[id=inputpicfile]').click();">Upload</a>
							</span>
						</div>
					</div>
				</div>
				<input id="inputpicfile" name="picture_file" type="file" style="display:none" />
				
				<div class="form-group">
				
					<label for="inputingr" class="col-lg-2 control-label">Ingredients</label>
					<div class="col-lg-10"><textarea id="inputingr" class="form-control" name="ingredients_data" placeholder="Milk (2 cups)"><s:iterator value="recipe.ingredients" var="ingredient">
${ingredient.name} (${ingredient.quantity})</s:iterator></textarea>
								</div></div>
				<div class="form-group">
					<label for="inputmeth" class="col-lg-2 control-label">Method</label>
					<div class="col-lg-10"><textarea id="inputmeth" class="form-control" placeholder="Tell us how to make it!" name="method">${recipe.method}</textarea>
				</div></div>
				<div class="form-group">
					<label for="inputserv" class="col-lg-2 control-label">Servings</label>
					<div class="col-lg-10"><input id="inputserv" type="text" class="form-control" placeholder="How many people does it serve?" name="servings" value="${recipe.servings}">
				</div></div>
				<div class="form-group">
					<label for="inputcat" class="col-lg-2 control-label">Category</label>
					<!-- TODO: Turn this into a dropdown? -->
					<div class="col-lg-10"><input id="inputcat" type="text" class="form-control" placeholder=Breakfast, Dessert, Beverage?" name="category" value="${recipe.category}">
				</div></div>
				<div class="form-group">
					<label for="inputcomp" class="col-lg-2 control-label">Complexity</label>
					<div class="col-lg-10">
						<select id="inputcomp" name="complexity" class="form-control">
							<c:forTokens items="Easy,Medium,Hard" delims="," var="diff">
								<c:choose>
									<c:when test="${diff.equals(recipe.complexity)}">
									<option value="${diff}" selected="selected">${diff}</option>
									</c:when>
									<c:otherwise>
									<option value="${diff}">${diff}</option>
									</c:otherwise>
								</c:choose>
							</c:forTokens>
						</select>
					</div>	
				</div>
			</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>&nbsp;
				<button type="submit" class="btn btn-primary pull-right" name="remix">Remix</button>&nbsp;
				<s:if test="loggedin && recipe.author.user_id == user.user_id">
					<button type="submit" class="btn btn-warning pull-right" name="update">Update</button>&nbsp;
					<button type="submit" class="btn btn-danger pull-right" name="delete">Delete</button>&nbsp;
				</s:if>
			</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->