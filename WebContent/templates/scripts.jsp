	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"
		type="text/javascript"></script>
	<!-- jQuery draggable -->
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script
		src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"
		type="text/javascript"></script>
		<script type="text/javascript">
function resetFormElement(e) {
	// http://stackoverflow.com/questions/1043957/clearing-input-type-file-using-jquery
	e.wrap('<form>').closest('form').get(0).reset();
	e.unwrap();
};
   $(document).ready(function () {
	$('input[id=inputpicfile]').change(function() {
		$('#inputpic').val($(this).val());
		$("#inputpic").prop("disabled", true);
	});
	var control = $("#inputpicfile");
	$("#clearbutton").on("click", function () {
	    resetFormElement($("#inputpicfile"));
		$("#inputpic").prop("disabled", false);
	});
   });
</script>