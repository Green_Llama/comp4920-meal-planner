<!-- Create Recipe Modal -->
<div class="modal fade" id="createRecipeModal" tabindex="-1" role="dialog"
	aria-labelledby="createRecipeModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">Upload a new recipe</h4>
			</div>
			<div class="modal-body">
				<div class="form-horizontal">
				<div class="form-group">
					<label for="inputname" class="col-lg-2 control-label">Name</label>
					<div class="col-lg-10"><input id="inputname" type="text" class="form-control" placeholder="The title of your recipe" name="name">
					</div>
				</div>
				<div class="form-group">
					<label for="inputdesc" class="col-lg-2 control-label">Description</label>
					<div class="col-lg-10"><input id="inputdesc" type="text" class="form-control" placeholder="A short description" name="description">
				</div></div>
				<div class="form-group">
					<label for="inputpic" class="col-lg-2 control-label">Picture</label>
					<div class="col-lg-10">
						<a class="btn btn-default pull-right" style="margin-left:5px;" id="clearbutton">Clear</a>
						<div class="input-group">
							<input id="inputpic" type="text" class="form-control" placeholder="A direct link to an image of your recipe" name="picture">
							<span class="input-group-btn">
								<a class="btn btn-primary" onclick="$('input[id=inputpicfile]').click();">Upload</a>
							</span>
						</div>
					</div>
				</div>
				<input id="inputpicfile" name="picture_file" type="file" style="display:none" />
				<div class="form-group">
				
					<label for="inputingr" class="col-lg-2 control-label">Ingredients</label>
					<div class="col-lg-10"><textarea id="inputingr" class="form-control" name="ingredients_data" placeholder="Milk (2 cups)"></textarea>
								</div></div>
				<div class="form-group">
					<label for="inputmeth" class="col-lg-2 control-label">Method</label>
					<div class="col-lg-10"><textarea id="inputmeth" class="form-control" placeholder="Tell us how to make it!" name="method"></textarea>
				</div></div>
				<div class="form-group">
					<label for="inputserv" class="col-lg-2 control-label">Servings</label>
					<div class="col-lg-10"><input id="inputserv" type="text" class="form-control" placeholder="How many people does it serve?" name="servings">
				</div></div>
				<div class="form-group">
					<label for="inputcat" class="col-lg-2 control-label">Category</label>
					<!-- TODO: Turn this into a dropdown? -->
					<div class="col-lg-10"><input id="inputcat" type="text" class="form-control" placeholder="What category does this fit under? Breakfast, Dessert, Beverage...?" name="category">
				</div></div>
				<div class="form-group">
					<label for="inputcomp" class="col-lg-2 control-label">Complexity</label>
					<div class="col-lg-10">
						<select id="inputcomp" name="complexity" class="form-control">
							<option value="Easy">Easy</option>
							<option value="Medium">Medium</option>
							<option value="Hard">Hard</option>
						</select>
					</div>	
				</div>
			</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>&nbsp;
				<button type="button" class="btn btn-primary pull-right" data-dismiss="modal" id="createRecipeSubmit">Create!</button>
			</div>
		</div>
		
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->