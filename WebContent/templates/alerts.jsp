

<s:iterator value="alerts" var="alert">
	<s:if test='#alert == "NO_RESULTS"'>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			No recipes matched your criteria.
			<!-- UI TODO: Why not try (random recipe link)? -->
		</div>
	</s:if>

	<s:elseif test='#alert == "RECIPE_STORE_FAILURE"'>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			Uh-oh! Something went wrong trying to save your recipe. Sorry!
		</div>
	</s:elseif>

	<s:elseif test='#alert == "MISSING_FIELD"'>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			Account not created; please fill in all mandatory fields.
		</div>
	</s:elseif>
	<s:elseif test='#alert == "INVALID_USERNAME"'>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			We couldn't create an account with that username. Your username must consist of only alphanumeric characters and be between 1 and 32 characters.
		</div>
	</s:elseif>
	<s:elseif test='#alert == "USER_EXISTS"'>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			Account not created; a user already exists with that username or email address.
		</div>
	</s:elseif>
	<s:elseif test='#alert == "PASSWORDS_DO_NOT_MATCH"'>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			Account not created; the passwords you entered do not match.
		</div>
	</s:elseif>
	<s:elseif test='#alert == "BAD_PASSWORD"'>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			Account not created; please select a password between 5 and 64 characters.
		</div>
	</s:elseif>
	<s:elseif test='#alert == "BAD_EMAIL"'>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			Account not created; please ensure your email address is entered correctly.
		</div>
	</s:elseif>
	<s:elseif test='#alert == "EMAIL_SENT"'>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			Thank you! A confirmation email has been sent to the supplied email address; please follow the instructions provided to complete your registration.
		</div>
	</s:elseif>
	<s:elseif test='#alert == "REGISTRATION_COMPLETE"'>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			You're all set! Click the log in button above to get started.
		</div>
	</s:elseif>
	
	<s:elseif test='#alert == "LOGIN_FAIL"'>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			Incorrect username or password.
		</div>
	</s:elseif>
	<s:else>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			${alert}
		</div>
	</s:else>
</s:iterator>