<!-- Registration Modal -->
<div class="modal fade" id="register" tabindex="-1" role="dialog"
	aria-labelledby="registrationModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">We just need a few details...</h4>
			</div>
			<form action="register" method="POST">
				<div class="modal-body">
					<div class="input-group">
						<span class="input-group-addon registration">Username</span>
						<input type="text" maxlength="32"
							class="form-control" name="username" placeholder="You'll use this to log in.">
						<span class="input-group-addon glyphicon glyphicon-pencil"></span>
					</div>
					<br />
					<div class="input-group">
						<span class="input-group-addon registration" style="border-bottom-left-radius:0px">Password</span>
						<input type="Password" maxlength="64"
							class="form-control" name="password" placeholder="This too."> 
						<span class="input-group-addon glyphicon glyphicon-pencil" style="border-bottom-right-radius:0px"></span>
					</div>

					<div class="input-group">
						<span class="input-group-addon registration" style="border-top-left-radius:0px;border-top:0px;">Confirm Password</span>
						<input type="Password" maxlength="64"
							class="form-control" name="confirm" placeholder="You'll only have to type it once, though.." style="border-top:0px"> 
						<span class="input-group-addon glyphicon glyphicon-pencil" style="border-top-right-radius:0px;border-top:0px;"></span>
					</div>
					<br />
					<div class="input-group">
						<span class="input-group-addon registration">Email Address</span>
						<input type="text" maxlength="255"
							class="form-control" name="email" placeholder="We won't spam you."> 
						<span class="input-group-addon glyphicon glyphicon-pencil"></span>
					</div>
					
					<br />
					<div class="input-group">
						<span class="input-group-addon registration">Name</span>
						<input type="text" maxlength="255"
							class="form-control" placeholder="First" name="firstname"> 
						<input type="text" maxlength="255"
							class="form-control" placeholder="Last (Optional)" name="lastname" style="border-top:0px;">
						<span class="input-group-addon glyphicon glyphicon-pencil"></span>
					</div>
				</div>
			
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary" >Sign Up</button>
						
			</div>
			</form>
		</div>
		
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<!-- /.modal -->
<!-- Login Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog"
	aria-labelledby="loginModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		<form action="login" method="POST">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">Welcome back!</h4>
			</div>
			<div class="modal-body">
					<input type="text"
						class="form-control" placeholder="Username or Email Address" name="username"> 
				<br />
					<input type="password"
						class="form-control" placeholder="Password" name="password"> 

			</div>
			<!-- I forgot my password :( -->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Log In</button>
			</div>
			</form>
		</div>
		
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->