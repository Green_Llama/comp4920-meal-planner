
<div class="navbar navbar-default navbar-inline-top" style="margin-bottom: 0px;">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span	class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">Scrumbs</a>
	</div>
	<div class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
			<li><s:a action="home">Home</s:a></li>
			<li><s:a action="results">Search</s:a></li>
			<li><s:a action="weeklyPlan">Weekly Plan</s:a></li>
			<s:if test="loggedin == true">
				<li><s:a action="createRecipe">Create Recipe</s:a></li>
			</s:if>
		</ul>
		<s:if test="loggedin != true">
		<div class="nav navbar-form navbar-right btn-group">
			<!-- Quick fix: the &nbsp; is a hack to make the buttons the same size. -->
			<a data-toggle="modal" href="#register" class="btn btn-primary"
				style="width: 100px;">Sign Up</a>
			<!-- <button type="button" class="btn btn-primary" style="width:100px;">Sign Up</button> -->
			<a data-toggle="modal" href="#login" class="btn btn-default">
				<small>Log</small>&nbsp;<small>in</small>
			</a>
		</div>
		</s:if>
		<s:else>
		<p class="nav navbar-nav navbar-text navbar-right" style="margin:15px 0px;">
			Hey there, <s:a action="profile">${user.username}</s:a>! Want to <s:a action="signout">sign out</s:a>?
		</p>
		</s:else>
		
	</div>
</div>
<s:if test="loggedin != true">
	<%@include file="usermodals.jsp" %>
</s:if>