<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Scrumbs - Your digital cookbook</title>
<%@include file="templates/headers.jsp" %>
</head>
<body>
	<div id="wrap">
		<!-- Static navbar -->
		<%@include file="templates/navbar.jsp" %>
		<div class="jumbotron">
			<div class="container">
			<%@include file="templates/alerts.jsp" %>
			
				<div class="page-header">
					<h1>
						Scrumbs <i><small>Your digital cookbook</small></i>
					</h1>
				</div>
				<p>
					Scrumbs is an online database for the best recipes on the web. Create, find and remix your recipes all in one place. <br />
					What are you waiting for, let's get cooking!
				</p>
				<br />
				<form action="results">
					<div class="container">
						<div class="input-group">
							<input type="text" name="searchQuery"
								placeholder="I'm hungry for some..." class="form-control" /> <span
								class="input-group-btn">
								<button type="submit" class="btn btn-primary btn-large">Search</button>
							</span>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!--  Featured recipes go here  -->
		<div class="container">
			<div class="row">
				<c:forEach var="recipe" items="${recipes}">
				<div class="col-md-4">
					<div class="well" style="style:min-height:100px;">
					<img src="${recipe.picture}" class="pull-right img-thumbnail" style="height: 100px;" 
										alt="" />
						<s:a action="recipe">
						<s:param name="recipeId">${recipe.recipe_id}</s:param>
						<h4 style="margin-bottom:3px;">${recipe.name}</h4>
						</s:a>
						<small>
						<c:choose>
						<c:when test="${recipe.rating == null}">
							<span style="color:#666666;">No ratings</span>
						</c:when>
						<c:otherwise>
							<c:set var="givenRating" value="${recipe.rating}" />
							<%@include file="templates/displayrating.jsp" %>
						</c:otherwise>
						</c:choose>
						</small>
						<br />
						${recipe.description}
						<br /> <br/>
						<i><small>by ${recipe.author.username}</small></i>
					</div>
				</div>
				</c:forEach>
			</div>
		</div>
	</div>
	<%@include file="templates/footer.jsp" %>
	<%@include file="templates/scripts.jsp" %>
</body>
</html>

