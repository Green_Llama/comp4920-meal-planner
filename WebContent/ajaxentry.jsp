<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<input type="hidden" name="plan_id" value="${plan_id}" />
<input type="hidden" name="meal" value="${meal}" />
<input type="hidden" name="day" value="${day}" />
<div class="list-group-item swappable"
	style="min-height: 70px; padding-bottom: 25px; position: relative">
	<input type="hidden" name="blahhh"
		value="${recipe_id}" />
	<s:a action="recipe">
		<s:param name="recipeId">${recipe_id}</s:param>
		<img src="${recipe.picture}"
			class="pull-right img-thumbnail" style="height: 50px;" alt="" />
		<b>${recipe.name}</b>
	</s:a>
		<div class="planentry-options">
			<small>
				<button data-toggle="tooltip" title="Remove" name="delete">
					<span class="glyphicon glyphicon-remove"></span>
				</button> &nbsp;
				<button data-toggle="tooltip" class="replace-popover" type="button"
					title="Replace"
					data-content='<input type="hidden" name="update" form="form-${day}-${meal}"/><input type="text" placeholder="Noodles?" name="recipe_name" form="form-${day}-${meal}" class="form-control" style="width:200px"/>'>
					<span class="glyphicon glyphicon-transfer"></span>
				</button> &nbsp;
				<button data-toggle="tooltip" title="Randomize" name="update">
					<span class="glyphicon glyphicon-refresh"></span>
				</button>
			</small>
		</div>
</div>