<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Search results - Scrumbs</title>
<%@include file="templates/headers.jsp" %>
<body>
	<div id="wrap">
		<!-- Static navbar -->
		<%@include file="templates/navbar.jsp" %>
		<br />
		<div class="container">
			<%@include file="templates/alerts.jsp" %>

			<h1>
				Search<s:if test='searchQuery != ""'> Results <small>'<s:property
							value="searchQuery" />'</s:if> <s:else><small>All Recipes</s:else>
				</small>
			</h1>
			<div class="container">
				<form action="results">

					<div class="input-group">
						<input type="text" name="searchQuery"
							placeholder="I'm hungry for some..." class="form-control" value="${searchQuery}"/> <span
							class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-large">Search</button>
						</span>
					</div>
					<div>
        				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="float:right;padding:8px;"> Add filters...</a>
        				<s:if test="author != null || category != null || complexity != null || rating != null || ingredients != null || servings != null || method != null || created != null">
        					<div id="collapseOne" class="panel-collapse collapse in">
        				</s:if>
        				<s:else>
        					<div id="collapseOne" class="panel-collapse collapse">
        				</s:else>
        					<div class="panel panel-default">
        						<div class="panel-body">
        						<br />
        							<div class="row">
	        							<div class="input-group col-md-4">
	        								<span class="input-group-addon filters">Created by</span>
	        								<input type="text" placeholder="anyone" class="form-control" name="author" value="${author}">
	        							</div>
	        							<div class="input-group col-md-3">
	        								<span class="input-group-addon filters">In category</span>
	        								<input type="text" placeholder="(any)" class="form-control" name="category" value="${category}">
	        							</div>
	        							<div class="input-group col-md-3">
	        								<span class="input-group-addon filters">Difficulty</span>
	        								<input type="text" placeholder="(any)" class="form-control" name="complexity" value="${complexity}">
	        							</div>
	        							<div class="input-group col-md-2">
	        								<span class="input-group-addon filters">Rated</span>
	        								<input type="text" placeholder="&gt; 3" class="form-control" name="rating" value="${rating}">
	        							</div>
        							</div>
        							<br />
        							<div class="row">
	        							<div class="input-group col-md-8">
	        								<span class="input-group-addon filters">Using</span>
	        								<input type="text" placeholder="Celeriac AND Coconut OR Lamb" class="form-control" name="ingredients" value="${ingredients}">
	        							</div>
	        							<div class="input-group col-md-4">
	        								<span class="input-group-addon filters">Serves</span>
	        								<input type="text" placeholder="(any)" class="form-control" name="servings" value="${servings}">
	        							</div>
        							</div>
        							<br />
        							<div class="row">
	        							<div class="input-group col-md-8">
	        								<span class="input-group-addon filters">In method</span>
	        								<input type="text" placeholder="Mix, mash, melt." class="form-control" name="method" value="${method}">
	        							</div>
	        							<div class="input-group col-md-4">
	        								<span class="input-group-addon filters" style="padding:0px;">
	        									<select name="created_range" class="form-control input-sm" style="width:95px;">
	        									<option value="before">Before</option>
	        									<s:if test='created_range != null && created_range.equals("after")'>
	        									<option value="after" selected="selected">After</option>
	        									</s:if>
	        									<s:else>
	        									<option value="after">After</option>
	        									</s:else>
	        								</select></span>
	        								<input type="text" placeholder="24/12/2012" class="form-control" name="created" value="${created}">
	        							</div>
        							</div>
        						</div>
        					</div>
        				</div>
        			</div>
				</form>
			</div>
			<br />
			<div class="list-group">
				<s:iterator value="results" var="recipe">
					<%@include file="templates/recipeentry.jsp"%>
				</s:iterator>

			</div>
		</div>
	</div>
	<%@include file="templates/footer.jsp" %>
	<%@include file="templates/scripts.jsp" %>
</body>
</html>