<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Generate a Weekly Plan - Scrumbs</title>
<%@include file="templates/headers.jsp" %>
</head>
<body>
	<div id="wrap">
		<!-- Static navbar -->
		<%@include file="templates/navbar.jsp" %>
		<br />
		<div class="container" id="pageContent">
			<%@include file="templates/alerts.jsp" %>
			<s:if test="loggedin && plan.user.user_id == user.user_id">
				<%@include file="templates/createRecipeModal.jsp" %>
				<!-- Sidebar -->
				<div id="usermeals" style="position:fixed; right:-270px; bottom:-20px;z-index:10;top:50px;width:300px;padding-left:35px;" class="well">
					<div id="slideout" style="height:100%;vertical-align:center;left:0px;width:35px;position:absolute;font-size:24px;color:#777777"><span class="glyphicon glyphicon-chevron-left"></span></div>
				<div style="text-align:right;padding-right:25px;">
					<h4>Your Meals</h4>
				</div>
				<div class="list-group" id="createRecipeParent">
					<c:forEach var="recipe" items="${userRecipes}">
						<%@include file="templates/recipeentry-minimal.jsp" %>
					</c:forEach>
					<a id="createRecipe" class="list-group-item" style="height:50px;" data-toggle="modal" href="#createRecipeModal">
						<div class="pull-right" style="padding-top:5px;">Create New Recipe&nbsp;</div>
						<span class="glyphicon glyphicon-plus" style="color:#777777; font-size:23px;"></span>
					</a>
				</div>
				<s:if test="savedRecipes.size() > 0">
					<div style="text-align:right;padding-right:25px;">
						<h4>Favourites</h4>
					</div>
					<div class="list-group">
						<c:forEach var="recipe" items="${savedRecipes}">
							<%@include file="templates/recipeentry-minimal.jsp" %>
						</c:forEach>
					</div>
				</s:if>
				<s:if test="ratedRecipes.size() > 0">
					<div style="text-align:right;padding-right:25px;">
						<h4>Rated</h4>
					</div>
					<div class="list-group">
						<c:forEach var="recipe" items="${ratedRecipes}">
							<%@include file="templates/recipeentry-minimal.jsp" %>
						</c:forEach>
					</div>
				</s:if>
				</div>
				
			</s:if>
			<div class="well">
				<!-- Plan action buttons -->
				<s:a action="weeklyPlan" cssClass="btn btn-default pull-right" cssStyle="float:right;margin-left:8px;">Random</s:a>
				<s:a action="weeklyPlan" cssClass="btn btn-default pull-right" cssStyle="float:right;margin-left:8px;">
					<s:param name="clear"></s:param>
					Empty
				</s:a>
				<s:if test="plan.user.user_id == user.user_id">
					<form action="deletePlan" method="POST" id="deletionform">
						<button type="submit" class="btn btn-danger btn-large pull-right">Delete this plan</button>
						<input type="hidden" name="plan_id" value="${plan.plan_id}" />
					</form>
				</s:if>
				<!-- /Plan action buttons -->
				<!-- Save plan forms -->
				<s:if test="loggedin">
					<form action="savePlan" method="POST">
					<!-- Load current plan into html -->
					<c:forEach var="day" items="${days}">
						<c:forEach var="meal" items="${mealtypes}">
							<input type="hidden" name='entries[${day}][${meal}]' value="${plan.entries[day][meal].recipe_id}" />
						</c:forEach>
					</c:forEach>
					<!-- /Load -->
					<s:if test="plan.plan_id == 0">	
						<div class="input-group">
							<input type="text" name="name" placeholder="Timothy the tasty meal plan" class="form-control" /> 
							<span class="input-group-btn">
								<button type="submit" class="btn btn-primary btn-large">Create Plan</button>
							</span>						
						</div>
					</s:if>
					<s:elseif test="plan.user.user_id != user.user_id">
						<div class="input-group">
							<input type="text" name="name" value="${plan.name} via ${plan.user.username}" class="form-control" /> 
							<span class="input-group-btn">
								<button type="submit" class="btn btn-primary btn-large">Save Plan</button>
							</span>
						</div>
					</s:elseif>
					</form>
				</s:if>
				<!-- /Save plan forms -->

				<s:if test="plan.user.user_id == user.user_id">
					<h3>${plan.name}</h3>
				</s:if>
				
				<div class="container">
				<table class="table">
					<thead>
						<tr>
							<td />
							<th style="text-align:center;"><h5>Breakfast</h5></th>
							<th style="text-align:center;"><h5>Lunch</h5></th>
							<th style="text-align:center;"><h5>Dinner</h5></th>
						</tr>
					</thead>
					<tbody id="planentries">
						<c:forEach var="day" items="${days}">
						<tr>
						<th>${day}</th>
							<c:forEach var="meal" items="${mealtypes}">
							<td>
							<form action="editPlan" method="POST" id="form-${day}-${meal}" class="form-inline">
							<input type="hidden" name="plan_id" value="${plan.plan_id}" />
							<input type="hidden" name="meal" value="${meal}" />
							<input type="hidden" name="day" value="${day}" />
							<c:choose>
								<c:when test="${plan.entries[day][meal] != null}">
									<div class="list-group-item swappable" style="min-height:70px;padding-bottom:25px;position:relative">
										<input type="hidden" name="blahhh" value="${plan.entries[day][meal].recipe_id}" />
										<s:a action="recipe">
											<s:param name="recipeId">${plan.entries[day][meal].recipe_id}</s:param>
								  			<img src= "${plan.entries[day][meal].picture}" class="pull-right img-thumbnail" style="height:50px;"/>
								 			<b>${plan.entries[day][meal].name}</b>
								  		</s:a>
								  		<s:if test="plan.user.user_id == user.user_id">
								  		<div class="planentry-options">
								  			<small>
								  				<button data-toggle="tooltip" title="Remove" name="delete">
								  					<span class="glyphicon glyphicon-remove"></span>
								  				</button>
								  				&nbsp;
								  				<button data-toggle="tooltip" class="replace-popover" type="button" title="Replace"
								  						data-content='<input type="hidden" name="update" form="form-${day}-${meal}"/><input type="text" placeholder="Noodles?" name="recipe_name" form="form-${day}-${meal}" class="form-control" style="width:200px"/>'
								  						>
								  					<span class="glyphicon glyphicon-transfer"></span>
								  				</button>
								  				&nbsp;
								  				<button data-toggle="tooltip" title="Randomize" name="update">
								  					<span class="glyphicon glyphicon-refresh"></span>
								  				</button>
								  			</small>
								  			</form>
								  		</div>
								  		</s:if>
									</div>
						  		</c:when>
						  		<c:otherwise>
						  			<div class="droppable">
							  		<div class="planentry-empty-options">
							  		<s:if test="plan.user.user_id == user.user_id">
							  				<br/>
												<button data-toggle="tooltip" class="replace-popover" type="button" title="Add"
									  						data-content='<input type="hidden" name="update" form="form-${day}-${meal}"/><input type="text" placeholder="Noodles?" name="recipe_name" form="form-${day}-${meal}" class="form-control" style="width:200px"/>'
									  						>
									  			<span class="glyphicon glyphicon-plus"></span>
									  		</button>
									  		&nbsp;
											<button data-toggle="tooltip" title="Randomize" name="update">
										  		<span class="glyphicon glyphicon-refresh"></span>
										  	</button>
						  			</s:if>
						  			</div>
						  			</div>
						  		</c:otherwise>
					  		</c:choose>
					  		</form>
							</td>
							</c:forEach>
						</tr>
						</c:forEach>
					</tbody>
				</table>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<s:if test="authorplans.size() > 0">
					<!-- List of meal plans -->
					<h3><s:a action="profile">
					<s:param name="username">${plan.user.username}</s:param>
					${plan.user.username}
					</s:a>'s meal plans</h3>
					<div class="list-group">
						<c:forEach var="p" items="${authorplans}">
							<c:choose>
								<c:when test="${p.plan_id == plan.plan_id}">
									<s:a action="weeklyPlan" cssClass="list-group-item active">
										<s:param name="plan_id">${p.plan_id}</s:param>
										${p.name}
									</s:a>
								</c:when>
								<c:otherwise>
									<s:a action="weeklyPlan" cssClass="list-group-item">
										<s:param name="plan_id">${p.plan_id}</s:param>
										${p.name}
									</s:a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</div>
					<!-- /List of meal plans -->
					</s:if>
					<s:if test="userplans.size() > 0">
					<!-- List of meal plans -->
					<h3>Your meal plans</h3>
					<div class="list-group">
						<c:forEach var="p" items="${userplans}">
							<c:choose>
								<c:when test="${p.plan_id == plan.plan_id}">
									<s:a action="weeklyPlan" cssClass="list-group-item active">
										<s:param name="plan_id">${p.plan_id}</s:param>
										${p.name}
									</s:a>
								</c:when>
								<c:otherwise>
									<s:a action="weeklyPlan" cssClass="list-group-item">
										<s:param name="plan_id">${p.plan_id}</s:param>
										${p.name}
									</s:a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</div>
					<!-- /List of meal plans -->
					</s:if>
				</div>
				<div class="col-md-6">
					<h3>Shopping List &nbsp;
					<s:if test="plan_id != null">
						<small><s:a action="shoppingList"><s:param name="plan_id">${plan_id}</s:param><span class="glyphicon glyphicon-print"></span> Printable</s:a></small>
					</s:if></h3>
					<ul class="list-group" id="shoppingList">
						<s:iterator value="shoppingList">
							<div class="list-group-item">
								${key}
								<div style="align:right;">
									<s:iterator value="value" var="k">
										<i><small>${k}</small></i> <br />
									</s:iterator>
								</div>
							</div>
						</s:iterator>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<%@include file="templates/footer.jsp" %>
	<%@include file="templates/scripts.jsp" %>
    <s:if test="loggedin && plan.user.user_id == user.user_id">
	<script type="text/javascript">
	var animating = false;
    $(document).ready(function () {
        $("button").tooltip({
          'selector': '',
          'placement': 'bottom'
        });
        $(".replace-popover").popover({
            	'placement': 'top',
            	'html':true,
        });
        function draggables_init() {
        	$( "#planentries div" ).draggable({revert:true, revertDuration:0, stack: "#planentries div" });
        	$( "#usermeals .recipe-entry" ).draggable({revert:true, revertDuration:0, stack: "#usermeals .recipe-entry" });
        	$( ".swappable" ).droppable({accept:"#planentries div, #usermeals .recipe-entry",
        	drop: function( event, ui ) {
        		if (ui.draggable[0].tagName == "A") {
        			var thisform = this.parentNode;
	   				var thispid = thisform.elements["plan_id"];
	   				var thismeal = thisform.elements["meal"];
	   				var thisday = thisform.elements["day"];
	   				var that = ui.draggable[0];
        			$(thisform).load("editPlan", {plan_id: thispid.value,
	    				recipe_id: that.getAttribute("data-recipe"),
	    				meal: thismeal.value,
	    				day: thisday.value,
	    				update: "true",
	    				"ajax": "true"}, function(){
	    					// Reload the shopping list
	    					$("#shoppingList").load("shoppingList", {"plan_id":"${plan.plan_id}", "ajax":"true"}, function(){});
	    		            $("button").tooltip({
	    		                'selector': '',
	    		                'placement': 'bottom'
	    		            });
	    		            $(".replace-popover").popover({
	    		                  	'placement': 'top',
	    		                  	'html':true,
	    		            });
	    		            draggables_init();
	    				});
        		} else {
                	var parent1 = $(this).parent();
                	var parent2 = $(ui.draggable).parent();
                	var pop1 = $(this).find(".planentry-options");
                	var pop2 = $(ui.draggable).find(".planentry-options");
                	pop1.detach();
                	pop2.detach();
                	$(this).detach();
                	$(ui.draggable).detach();
                	$(this).append(pop2);
                	$(ui.draggable).append(pop1);
                	parent1.append($(ui.draggable));
                	parent2.append($(this));
                	// commit changes.
                	var thisform = $(this)[0].parentNode;
                	
                	$.post("editPlan", {plan_id: thisform.elements["plan_id"].value,
                						recipe_id: thisform.elements["blahhh"].value,
                						meal: thisform.elements["meal"].value,
                						day: thisform.elements["day"].value,
                						update: "true",
                						"ajax": "true"}, function(){});
                	thisform = $(ui.draggable)[0].parentNode;
                	$.post("editPlan", {plan_id: thisform.elements["plan_id"].value,
        				recipe_id: thisform.elements["blahhh"].value,
        				meal: thisform.elements["meal"].value,
        				day: thisform.elements["day"].value,
        				update: "true",
        				"ajax": "true"}, function(){});
        		}

        	}
        	});
        	$( ".droppable" ).droppable({accept:"#planentries div, #usermeals .recipe-entry",
            	drop: function( event, ui ) {
            		if (ui.draggable[0].tagName == "A") {
            			var thisform = this.parentNode;
    	   				var thispid = thisform.elements["plan_id"];
    	   				var thismeal = thisform.elements["meal"];
    	   				var thisday = thisform.elements["day"];
    	   				var that = ui.draggable[0];
            			$(thisform).load("editPlan", {plan_id: thispid.value,
    	    				recipe_id: that.getAttribute("data-recipe"),
    	    				meal: thismeal.value,
    	    				day: thisday.value,
    	    				update: "true",
    	    				"ajax": "true"}, function(){
    	    					$("#shoppingList").load("shoppingList", {"plan_id":"${plan.plan_id}", "ajax":"true"}, function(){});
    	    					$("button").tooltip({
    	    		                'selector': '',
    	    		                'placement': 'bottom'
    	    		            });
    	    		            $(".replace-popover").popover({
    	    		                  	'placement': 'top',
    	    		                  	'html':true,
    	    		            });
    	    		            draggables_init();
    	    				});
            		} else {
		            	var parent1 = $(this).parent();
		            	var parent2 = $(ui.draggable).parent();
		            	$(this).detach();
		            	$(ui.draggable).detach();
		            	parent1.append($(ui.draggable));
		            	parent2.append($(this));
		            	// commit changes.
		            	var thisform = this.parentNode;
		            	var thatform = $(ui.draggable)[0].parentNode;
		            	
		   				// Save inputs
		   				var thispid = thisform.elements["plan_id"];
		   				var thismeal = thisform.elements["meal"];
		   				var thisday = thisform.elements["day"];
		   				var thatpid = thatform.elements["plan_id"];
	    				var thatrid = thatform.elements["blahhh"];
	    				var thatmeal = thatform.elements["meal"];
	    				var thatday = thatform.elements["day"];
		   				var thisinp = thisform.elements;
		   				var thatinp = thatform.elements;
		   				$(thisinp).each(function(i, e) {
		   					$(e).detach();
		   				});
		   				$(thatinp).each(function(i, e) {
		   					$(e).detach();
		   				});
		            	$(thisform).load("editPlan", {plan_id: thispid.value,
		            						meal: thismeal.value,
		            						day: thisday.value,
		            						"delete": "true",
		            						"ajax": "true"}, function(){
		            				            $("button").tooltip({
		            				                'selector': '',
		            				                'placement': 'bottom'
		            				            });
		            				            $(".replace-popover").popover({
		            				                  	'placement': 'top',
		            				                  	'html':true,
		            				            });
		            				            draggables_init();
		            						});
		            	$(thatform).load("editPlan", {plan_id: thatpid.value,
		    				recipe_id: thatrid.value,
		    				meal: thatmeal.value,
		    				day: thatday.value,
		    				update: "true",
		    				"ajax": "true"}, function(){
		    		            $("button").tooltip({
		    		                'selector': '',
		    		                'placement': 'bottom'
		    		            });
		    		            $(".replace-popover").popover({
		    		                  	'placement': 'top',
		    		                  	'html':true,
		    		            });
		    		            draggables_init();
		    				});
            		}
		         }});
        }
        draggables_init();
        $("#usermeals").hover(
            function(){
            	if (animating == false) {
	            	animating = true;
	                $("#usermeals").animate({right:-30}, 250, function(){animating=false;});
            	}
            },
            function(){
            	animating = true;
                $("#usermeals").animate({right:-270}, 250, function(){animating=false;});
            }
        );
        $("#createRecipeSubmit").click(function(e) {
        	var cr = $("#createRecipe");
        	cr.detach();
        	$.post("saveRecipe", {"name": document.getElementById("inputname").value,
        		"description": document.getElementById("inputdesc").value,
        		"picture": document.getElementById("inputpic").value,
        		"ingredients_data": document.getElementById("inputingr").value,
        		"method": document.getElementById("inputmeth").value,
        		"servings": document.getElementById("inputserv").value,
        		"category": document.getElementById("inputcat").value,
        		"complexity": document.getElementById("inputcomp").value,
        	    "ajax":"true"}, function(data){
        		$("#createRecipeParent").append(data);
        		$("#createRecipeParent").append(cr);
        	});
        	animating = true;
            $("#usermeals").animate({right:-30}, 250, function(){
            	setTimeout(function(){
            		$("#usermeals").animate({right:-270}, 250, function(){animating=false;});
            	}, 2000);
            });
        }
        );

      });
	</script>
	</s:if>
</body>
</html>