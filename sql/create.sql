-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: comp4920
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.13.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `equipment`
--

DROP TABLE IF EXISTS `equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment` (
  `recipe` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  KEY `recipe` (`recipe`),
  CONSTRAINT `equipment_ibfk_1` FOREIGN KEY (`recipe`) REFERENCES `recipes` (`recipe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment`
--

LOCK TABLES `equipment` WRITE;
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS `ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients` (
  `recipe` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  KEY `recipe` (`recipe`),
  CONSTRAINT `ingredients_ibfk_1` FOREIGN KEY (`recipe`) REFERENCES `recipes` (`recipe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients`
--

LOCK TABLES `ingredients` WRITE;
/*!40000 ALTER TABLE `ingredients` DISABLE KEYS */;
INSERT INTO `ingredients` VALUES (7,'teabag','1'),(7,'water','1 cup'),(7,'sugar','(optional)'),(7,'milk','(optional)'),(1,'Assembler','1'),(1,'programmer','1 underpaid'),(3,'chocolates','3 whole'),(3,'essence of Brownie','1 pan'),(3,'brownie chef','1'),(4,'pasta salad','1'),(5,'rump steak','1'),(6,'boiling water','1 bowl\'s worth'),(6,'packet of noodles','1'),(1,'carrot','1'),(1,'pear','2'),(15,'ice','4'),(15,' cream',' some'),(16,'olive oil','1 tbsp'),(16,' freshly squeezed lemon juice',' 1 tbsp'),(16,' roasted red bell pepper (from jar) cut into strips',' 1/8 cup'),(16,' champagne vinegar or white wine vinegar',' 1 tsp'),(16,' fresh chopped parsley',' 2 tbsp'),(16,' freshly ground black pepper',' dash'),(16,' garlic pressed or minced',' 1 clove'),(16,' fresh spinach leaves',' 1 cup'),(16,' cooked brown rice',' 1/2 cup'),(16,' white tuna in water (drained)',' 6 oz'),(16,' cucumber chopped',' 1/2'),(16,' chopped red onion',' 1/4 cup'),(16,' diced tomato',' 1/2 cup'),(16,' salt',' dash'),(17,'cook quinoa cooled','2 cups'),(17,' feta crumbled',' 100g'),(17,' eggs whisked',' 4'),(17,' carrot peeled',' 1'),(17,' pine nuts toasted',' 55g'),(17,' fresh breadcrumps',' 25g'),(17,' baby spinach',' 20g'),(18,'eggs','2'),(18,'tomatoes','1'),(18,'oil','2 tbsp'),(18,' shallots',' 1'),(19,'chicken thigh fillets trimmed halved','800g'),(19,' olive oil',' 2 tsp'),(19,' firmly packed fresh basil leaves',' 2 cups'),(19,'pine nuts toasted',' 1/4 cup'),(19,' garlic cloves',' 2'),(19,' finely grated parmesan cheese',' 1/2 cup'),(19,' lemon juice',' 2 tbsp'),(19,' olive oil',' 1/3 cup'),(21,'small avocados skinless','2'),(21,' dried red chili flakes',' 1/4 tsp'),(21,' lemon juice',' 2 tsp'),(21,' whole grain English muffins',' 4'),(21,' baby spinach',' 50g'),(21,' sliced smoke salmon',' 100g'),(21,' lemon wedges',' 1'),(22,'olive oil','1 tbsp'),(22,' chopped onion',' 1'),(22,' chopped bacon',' 3 rashers'),(22,' sliced mushrooms',' 200g'),(22,' fresh thyme leaves',' 2tsp'),(22,' ready-rolled butter puff pastry',' 3 sheets'),(22,' eggs',' 2'),(22,' cream',' 1 cup'),(22,' baby spinach leaves',' some'),(26,'natural muesli','1 cup'),(26,' honey',' 2 tbsp'),(26,' plain flour',' 1 cup'),(26,' sachet instant dry yeast',' 8g'),(26,' caster sugar',' 2 tsp'),(26,' butter',' 30g'),(26,' strawberry jam',' 1/2 cup'),(26,' fresh strawberries',' 150g'),(26,' fresh or frozen blueberries',' 100g'),(26,' fresh or frozen raspberries',' 50g'),(26,' icing sugar mixture and Greek-style yoghurt',' to serve'),(27,'barbecued or roast chicken','1'),(27,' green onion','3'),(27,' carrot','1'),(27,' salted cashews','1/2 cup'),(27,' bread rolls','to serve'),(27,' olive oil','2 tbsp'),(27,' sesame oil','2 tsp'),(27,' light soy sauce','2 tbsp'),(27,' rice vinegar','1 tbsp'),(27,' caster sugar','1 tsp'),(27,' garlic clove','1'),(28,'macaroni pasta','2 1/2'),(28,' butter',' 60g'),(28,' plain flour',' 1/4 cup'),(28,' milk',' 2 cups'),(28,' grated cheddar cheese',' 1 1/2 cups'),(28,' shaved ham',' 150g'),(28,' flat-leaf parsley leaves',' 2 tbsp'),(29,'penne','400g'),(29,' olive oil',' 1/2 cup'),(29,' garlic cloves',' 6'),(29,' rocket leaves',' 100g'),(29,' juice of large lemons',' 2'),(29,' grated parmesan',' 1 cup'),(29,' cooked peeled prawns',' 500g'),(30,'bacon and cheese topped roll','1'),(30,' can baked beans',' 140g'),(30,' celery stick sliced',' 1/2'),(30,' grated cheddar',' 2 tbsp'),(31,'shredded cooked chicken','80g'),(31,' mayonnaise',' 1 tbsp'),(31,' Helga\'s White Sourdough Seed Sensation',' 2 slices'),(31,' small pear sliced',' 1/4'),(33,'vine-ripened tomatoes','12'),(33,' ice',' to serve'),(33,' zucchini',' 1'),(33,' baby eggplant',' 1'),(33,' small red capsicum',' 1'),(33,' olive oil',' 1/3 cup'),(33,' garlic cloves',' 3'),(33,' balsamic vinegar',' 150mo'),(33,' couscous',' 3/4 cup'),(33,' chicken stock boiling',' 1 cup'),(33,' spring onions',' 4'),(33,' chopped basil',' 2 tbsp'),(33,' chopped flat-leaf parsley leaves',' 2 tbsp'),(38,'Test','Test'),(47,'test','test'),(48,'test','test'),(48,'test2','test');
/*!40000 ALTER TABLE `ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipes`
--

DROP TABLE IF EXISTS `recipes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipes` (
  `recipe_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `picture` varchar(255) DEFAULT NULL,
  `method` text,
  `author` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `servings` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `complexity` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  PRIMARY KEY (`recipe_id`),
  KEY `author` (`author`),
  KEY `parent` (`parent`),
  CONSTRAINT `recipes_ibfk_1` FOREIGN KEY (`author`) REFERENCES `users` (`user_id`),
  CONSTRAINT `recipes_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `recipes` (`recipe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipes`
--

LOCK TABLES `recipes` WRITE;
/*!40000 ALTER TABLE `recipes` DISABLE KEYS */;
INSERT INTO `recipes` VALUES (1,'Spaghetti Code','Lots of GOTO.','http://sourcemaking.com/files/sm/images/spagett.jpg','PRINT \'HELLO\'; GOTO 1;',1,'2013-10-09 03:45:42','Infinite Servings','Digital','Easy',NULL),(3,'Brownies','Delicious chocolate brownies!','http://www.elanaspantry.com/blog/wp-content/uploads/2009/02/dsc_8204brownies.jpg','1. Mix ingredients. 2. Put in oven. 3. Serve',1,'2013-10-08 11:41:49','4','Dessert','Medium',1),(4,'Pasta salad','A cool creamy pasta salad','http://www.taste.com.au/images/recipes/sfi/2006/02/371_l.jpg','I dunno. Put it all in a bowl or some shit!',1,'2013-10-08 12:12:44','2','Salad','Easy',1),(5,'Rump steak','Tasty and juicy','http://guyawford.files.wordpress.com/2011/05/rump-steak-1.jpg','1. Cook 2. Eat',1,'2013-10-08 12:15:02','1','Main','Medium',1),(6,'Instant Noodles','Quick, cheap, and delicious.','http://4.bp.blogspot.com/-dYXRu5AODR0/Tdd1WKNefhI/AAAAAAAAACQ/52IwRAgMEPE/s1600/shin_ramyun.jpg','1. Boil water.\r\n2. Add noodles.\r\n3. Wait.',1,'2013-10-08 22:55:12','1','Cheap Meals','Easy',1),(7,'Tea','A refreshing cup of tea. Mmmm.','http://blog.zansaar.com/wp-content/uploads/2012/08/Green-Tea-weight-loss.jpg','1. Boil water.\r\n2. Pour into a cup\r\n3. Add teabag. Let tea steep until of satisfactory consistency.\r\n4. Add sugar and dairy to taste.',1,'2013-10-08 23:01:08','1','Hot Beverage','Easy',1),(15,'Peter\'s awesome ice cream','I scream for ice scream','http://blog.positscience.com/wp-content/uploads/2013/08/ice-cream3.jpg','1.Put the ice with the cream.\r\n2. now you have ice cream\r\n3. cool. cold!',25,'2013-10-16 02:36:13','1','Dessert','Easy',NULL),(16,'Mediterranean Tuna Salad','','http://www.taste.com.au/images/recipes/sfi/2008/03/17996_l.jpg','1: Dressing: In a small mixing bowl, whisk together olive oil, lemon juice, bell peppers, vinegar, parsley, pepper, salt, and garlic.\r\n2: For each serving, place spinach on a plate and top with brown rice, tuna, cucumber, onion, and tomato. Drizzle with dressing.',28,'2013-10-21 13:05:35','2','Salad','medium',NULL),(17,'Melissa\'s quinoa cakes','Quinoa is ideal for making fritters filled with feta, pine nuts and juicy sultanas.','http://www.taste.com.au/images/recipes/agt/2012/06/29943_l.jpg','Step 1\r\nCombine the quinoa, feta, egg, carrot, pine nuts, breadcrumbs, spinach, mint, sultanas, Moroccan seasoning, honey and lemon rind in a bowl. Season.\r\nStep 2\r\nGrease six egg rings and a large frying pan. Heat pan over medium-low heat. Place egg rings in pan. Divide half the quinoa mixture between egg rings. Cook for 3-4 minutes each side or until golden. Remove quinoa cakes from egg rings. Cover to keep warm. Repeat to make 12 cakes.\r\nStep 3\r\nServe the quinoa cakes with pumpkin dip.',28,'2013-10-21 13:15:54','4','Dessert','Easy',NULL),(18,'Stir-fried eggs and tomatoes','A traditional Chinese cuisine','http://www.chinadaily.com.cn/dfpd/attachement/jpg/site1/20111209/0013729e46ae104baedd2d.jpg','1. 1 tbsp oil heated in a non-stick pan\r\n2. Make egg scramble with the hot oil.\r\n3, Take out the scramble, add another tbsp oil heated\r\n4. Fry shallots(diced) and tomatoes until they are soft \r\n5. Add eggs, and salt or sugar',28,'2013-10-21 13:22:21','1','Dinner','Easy',NULL),(19,'Pesto Chicken','','http://www.taste.com.au/images/recipes/sfi/2008/04/19410_l.jpg','Step 1\r\nMake basil pesto: Place basil, pine nuts, garlic and parmesan in a processor. Process until roughly chopped. Add lemon juice. Process until combined. With motor running, add oil in a thin, steady stream. Process until combined. Transfer to a bowl. Season with pepper. Cover. Refrigerate until required.\r\nStep 2\r\nPreheat a barbecue plate or chargrill on medium. Brush chicken with oil. Season with salt and pepper. Cook for 3 to 4 minutes each side or until cooked through.\r\nStep 3\r\nTop chicken with pesto. Serve.',28,'2013-10-21 14:38:21','8','Dinner','Easy',NULL),(21,'Spicy avocado & salmon toasted muffin','This English muffin recipe ensures every bite is packed full of flavours and is nutritionally balanced. Add an oozy poached egg to the mix if you have an extra few minutes. Brought to you by the Taste team and Australian Avocados.','http://www.taste.com.au/images/recipes/tas/2013/10/33988_l.jpg','Step 1\r\nPlace avocado flesh in a medium size bowl with the chilli flakes and lemon juice. Mash roughly with a fork, season to taste with sea salt and freshly ground black pepper.\r\nStep 2\r\nToast muffins until golden. Top with mashed avocado and divide the smoked salmon and spinach leaves evenly over half the toasted muffins. Serve with remaining muffin half and a wedge of lemon. Tip: try adding a poached or fried egg on top.',28,'2013-10-22 12:17:20','4','Breakfast','Easy',NULL),(22,'Breakfast pies','These hearty individual bacon and mushroom pies will have everyone dashing to the breakfast table.','http://www.taste.com.au/images/recipes/sfi/2003/11/9195_l.jpg','Step 1\r\nPreheat oven to 220°C. Place a baking tray into oven.\r\nStep 2\r\nHeat oil in a frying pan over medium-high heat. Add onion and bacon. Cook for 4 minutes. Add mushrooms and thyme. Cook for 3 minutes. Season. Cool.\r\nStep 3\r\nCut 2 x 14cm circles from each pastry sheet. Ease circles into 6 x 12cm (base) loose-base tart pans. Spoon bacon mixture over pasty. Place pans onto hot tray.\r\nStep 4\r\nWhisk eggs, cream, and salt and pepper together. Pour over bacon mixture.\r\nStep 5\r\nBake for 30 minutes or until puffed and golden. Serve with baby spinach leaves.',28,'2013-10-22 12:25:38','6','Breakfast','Medium',NULL),(23,'Arabic breakfast','This casual assembly of eggs, ricotta and chargrilled vegetables makes an extraordinary weekend brunch.','http://www.taste.com.au/images/recipes/del/2010/10/26546_l.jpg','Step 1\r\nPreheat a chargrill pan or barbecue grill to medium-high heat. Cut the eggplant lengthways into 8 thin slices, then lightly brush each side with olive oil. Grill for 2 minutes on each side or until charred. Set aside to cool slightly.\r\nStep 2\r\nStir the chopped mint into the ricotta until well combined, then season well. When eggplant is cooled but still soft, spread a heaped tablespoon of ricotta mixture at the end of 1 slice and roll up to enclose. Continue with remaining eggplant and ricotta mixture. Set aside.\r\nStep 3\r\nPlace the eggs in a pan, cover with hot water and bring to boil. Reduce heat to medium and simmer for 5-6 minutes for soft-boiled, then remove and plunge into iced water. Peel, then halve.\r\nStep 4\r\nTo serve, divide the eggplant rolls, eggs, tomatoes, cucumber and olives among serving plates. Sprinkle the eggs with zaatar or sumac. Crumble over feta and drizzle with honey. Garnish with extra mint leaves and drizzle with extra virgin olive oil.',28,'2013-10-22 12:51:54','4','Breakfast','Medium',NULL),(24,'Arabic breakfast','This casual assembly of eggs, ricotta and chargrilled vegetables makes an extraordinary weekend brunch.','http://www.taste.com.au/images/recipes/del/2010/10/26546_l.jpg','Step 1\r\nPreheat a chargrill pan or barbecue grill to medium-high heat. Cut the eggplant lengthways into 8 thin slices, then lightly brush each side with olive oil. Grill for 2 minutes on each side or until charred. Set aside to cool slightly.\r\nStep 2\r\nStir the chopped mint into the ricotta until well combined, then season well. When eggplant is cooled but still soft, spread a heaped tablespoon of ricotta mixture at the end of 1 slice and roll up to enclose. Continue with remaining eggplant and ricotta mixture. Set aside.\r\nStep 3\r\nPlace the eggs in a pan, cover with hot water and bring to boil. Reduce heat to medium and simmer for 5-6 minutes for soft-boiled, then remove and plunge into iced water. Peel, then halve.\r\nStep 4\r\nTo serve, divide the eggplant rolls, eggs, tomatoes, cucumber and olives among serving plates. Sprinkle the eggs with zaatar or sumac. Crumble over feta and drizzle with honey. Garnish with extra mint leaves and drizzle with extra virgin olive oil.',28,'2013-10-22 12:52:42','4','Breakfast','Medium',NULL),(25,'Arabic breakfast','This casual assembly of eggs, ricotta and chargrilled vegetables makes an extraordinary weekend brunch.','http://www.taste.com.au/images/recipes/del/2010/10/26546_l.jpg','Step 1\r\nPreheat a chargrill pan or barbecue grill to medium-high heat. Cut the eggplant lengthways into 8 thin slices, then lightly brush each side with olive oil. Grill for 2 minutes on each side or until charred. Set aside to cool slightly.\r\nStep 2\r\nStir the chopped mint into the ricotta until well combined, then season well. When eggplant is cooled but still soft, spread a heaped tablespoon of ricotta mixture at the end of 1 slice and roll up to enclose. Continue with remaining eggplant and ricotta mixture. Set aside.\r\nStep 3\r\nPlace the eggs in a pan, cover with hot water and bring to boil. Reduce heat to medium and simmer for 5-6 minutes for soft-boiled, then remove and plunge into iced water. Peel, then halve.\r\nStep 4\r\nTo serve, divide the eggplant rolls, eggs, tomatoes, cucumber and olives among serving plates. Sprinkle the eggs with zaatar or sumac. Crumble over feta and drizzle with honey. Garnish with extra mint leaves and drizzle with extra virgin olive oil.',28,'2013-10-22 12:52:48','4','Breakfast','Medium',NULL),(26,'Berry breakfast tart','Get your Sunday off to a sweet start with this delicious recipe.','http://www.taste.com.au/images/recipes/sfi/2007/11/18353_l.jpg','Step 1\r\nCombine muesli and honey in a bowl.\r\nStep 2\r\nPreheat oven to 220°C. Sift flour into a bowl. Add yeast, sugar and butter. Using fingertips, rub butter into flour mixture until mixture resembles breadcrumbs. Add 1/2 cup of warm water to flour mixture. Mix until dough comes together, adding more water if necessary. Turn dough onto a lightly-floured surface. Knead for 8 minutes or until elastic. Place in a lightly-oiled bowl. Cover with plastic wrap. Set aside in a warm place for 25 to 30 minutes or until dough doubles in size.\r\nStep 3\r\nLightly grease a large baking tray. Turn dough onto a lightly-floured surface. Roll out dough to form a 2cm-thick, 20cm x 30cm rectangle. Place on prepared tray. Spread jam over dough. Top with fruit, and muesli mixture. Bake for 15 minutes or until base is crisp. Dust with icing sugar. Serve with yoghurt.',28,'2013-10-22 13:03:05','6','Breakfast','Easy',NULL),(27,'Asian-style chicken salad rolls','Liven up lunches with these tasty chicken roll bundles inspired by the flavours of Asia.','http://www.taste.com.au/images/recipes/sfi/2003/11/9176_l.jpg','Step 1\r\nRemove skin from chicken and discard. Remove chicken from bones and discard bones. Shred chicken. Place into a bowl.\r\nStep 2\r\nMake dressing: Combine all ingredients in a jug. Whisk with a fork until well combined.\r\nStep 3\r\nPour dressing over chicken. Season with salt and pepper. Toss well to combine. Place into an airtight container. Place onions, carrot and nuts into separate airtight containers.\r\nStep 4\r\nWhen ready to serve, add onions, carrot and nuts to chicken. Toss. Spoon into bread rolls or serve as a salad.',28,'2013-10-22 13:41:01','6','Lunch','',NULL),(28,'Cheesy ham and macaroni bake','A new year of school means a new year of school lunches and this pasta bake is just the thing to make-ahead, freeze and defrost when you need it!','http://www.taste.com.au/images/recipes/sfi/2006/02/366_l.jpg','Step 1\r\nPreheat oven to 180°C. Cook pasta in a large saucepan of boiling, salted water, following packet directions until tender.\r\nStep 2\r\nMeanwhile, melt butter in a saucepan over medium heat. When foaming, add flour. Cook, stirring, for 30 seconds. Remove from heat. Add half the milk and whisk until smooth. Add remaining milk and whisk to combine. Return pan to heat. Cook, stirring with a wooden spoon, for 3 minutes or until sauce comes to the boil and thickens. Stir in 1 cup cheese. Season with salt and pepper.\r\nStep 3\r\nDrain pasta and return to saucepan. Add ham, parsley and white sauce. Stir to combine. Spoon into a 7-cup capacity baking dish. Sprinkle with remaining cheese. Bake for 25 to 30 minutes or until golden. Set aside to cool completely. Cut into 8 even slices.',28,'2013-10-22 13:43:04','8','Lunch','Easy',NULL),(29,'Pasta salad with roasted garlic pesto and prawns','Summer lunches are best enjoyed with a big bowl of fresh seafood pasta and a nice chilled glass of wine.','http://www.taste.com.au/images/recipes/del/2004/12/4624_l.jpg','Step 1\r\nCook the penne according to packet instructions. Drain, toss in a little olive oil and refrigerate.\r\nStep 2\r\nPreheat the oven to 170°C.\r\nStep 3\r\nPlace garlic on a sheet of foil, drizzle with a little oil, wrap in foil and roast for 20 minutes. Cool slightly and squeeze cloves out of skins into the bowl of a food processor. Add rocket, the 1/2 cup oil and lemon juice, and process to combine. Transfer to a bowl, stir in parmesan and season with salt and pepper.\r\nStep 4\r\nPlace the pasta in a large bowl with 4 tablespoons pesto (remaining pesto can be kept in the fridge for 2 days) and zest. Toss to combine and pile onto plates. Top with prawns and extra rocket and serve with lemon wedges. If desired, top salad with some remaining pesto, combined with good-quality mayonnaise.',28,'2013-10-22 13:45:49','4','Lunch','Easy',NULL),(30,'Bean machine','If you have the same lunch every day, or the kids\' sandwiches keep coming back, take heart. With this new idea, lunch will never be the same again!','http://www.taste.com.au/images/recipes/agt/2010/02/24080_l.jpg','Halve 1 bacon and cheese topped roll. Top the base with a 140g can baked beans. Top with 1/2 celery stick, sliced; and 2 tbs grated cheddar. Top with remaining roll.',28,'2013-10-22 13:59:58','1','Lunch','Easy',NULL),(31,'Chicken little','If you have the same lunch every day, or the kids\' sandwiches keep coming back, take heart. With this new idea, lunch will never be the same again!','http://www.taste.com.au/images/recipes/agt/2010/02/24064_l.jpg','Combine 80g shredded cooked chicken and 1 tbs mayonnaise. Spread over 1 slice Helga’s White Sourdough Seed Sensation. Top with 1/4 small pear, sliced; alfalfa sprouts; and another slice of bread. Cut in half.',28,'2013-10-22 14:03:27','1','Lunch','Easy',NULL),(32,'Egg and rice parcels','These delicious parcels are perfect for kids lunches!','http://www.taste.com.au/images/recipes/sfi/2010/07/25138_l.jpg','Step 1\r\nCook rice following absorption method on packet. Set aside to cool slightly. Meanwhile, heat oil in a large frying pan over medium heat. Add onion and garlic. Cook, stirring, for 2 minutes or until tender. Add carrot and capsicum. Cook, stirring, for 2 minutes or until vegetables start to soften.\r\nStep 2\r\nAdd rice. Cook, stirring, for 1 minute or until combined. Add peas and corn, soy sauce and kecap manis. Cook, stirring, for 2 minutes or until combined. Transfer to a bowl. Cover with foil to keep warm.\r\nStep 3\r\nCrack 1 egg into a bowl. Whisk with a fork. Spray a 16cm (base) non-stick frying pan with oil. Heat over medium-high heat. Pour egg into pan. Swirl to coat base of pan. Cook for 30 seconds or until set. Transfer to a plate. Cover to keep warm. Repeat with remaining eggs.\r\nStep 4\r\nPlace 1 omelette on a flat surface. Place 1/4 cup rice mixture in centre. Fold in corners to form a parcel. Transfer, seam side down, to a plate. Repeat with remaining omelettes and rice mixture. Serve.',28,'2013-10-22 14:08:01','8','Lunch','Easy',NULL),(33,'Tomatoes stuffed with roast vegetable couscous','These tasty stuffed tomatoes are a brilliant idea for alfesco Christmas lunches.','http://www.taste.com.au/images/recipes/del/2009/12/23827_l.jpg','Step 1\r\nPreheat oven to 180°C. Line a baking tray.\r\nStep 2\r\nCut a small cross in the base of each tomato. Plunge each into boiling water for 30 seconds, then remove and refresh immediately in iced water. Peel, then chill for 15 minutes (to firm up). Cut off and reserve the tops. Carefully hollow out tomatoes with a teaspoon, season with salt and pepper, then leave to drain upside-down on paper towel.\r\nStep 3\r\nMeanwhile, toss the vegetables in 2 tablespoons oil with garlic. Season, then spread onto the lined baking tray. Roast for 15 minutes, turning once, until slightly charred. Toss with balsamic vinegar.\r\nStep 4\r\nAdd the couscous, 1 tablespoon oil and 1 teaspoon salt to the hot stock. Cover and stand for 10 minutes. Fluff up with a fork, then stir through vegetables, spring onion and herbs. Season with salt and pepper.\r\nStep 5\r\nFill tomatoes with couscous. Return the tops and drizzle with remaining oil. Serve at room temperature or return to the oven for 10 minutes until warmed through.',28,'2013-10-22 14:13:39','12','Lunch','Easy',NULL),(38,'Test','Test','test','test',24,'2013-10-22 16:14:35','test','test','Hard',NULL),(39,'test','test','tets','test',24,'2013-10-22 16:37:32','test','test','Easy',NULL),(40,'test','test','tets','test',24,'2013-10-22 16:39:01','test','test','Easy',NULL),(41,'test','test','tets','test',24,'2013-10-22 16:39:18','test','test','Easy',NULL),(42,'test','test','tets','test',24,'2013-10-22 16:39:23','test','test','Easy',NULL),(43,'test','test','tets','test',24,'2013-10-22 16:40:42','test','test','Easy',NULL),(44,'test','test','tets','test',24,'2013-10-22 16:41:13','test','test','Easy',NULL),(45,'test','test','tets','test',24,'2013-10-22 16:41:18','test','test','Easy',NULL),(46,'test','test','test','teste',24,'2013-10-22 16:43:14','stesdf','dfs','Easy',NULL),(47,'test','test','test','teste',24,'2013-10-22 16:46:18','stesdf','dfs','Easy',NULL),(48,'test','test','test','teste',24,'2013-10-22 16:46:29','stesdf','dfs','Easy',NULL);
/*!40000 ALTER TABLE `recipes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saved`
--

DROP TABLE IF EXISTS `saved`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved` (
  `user` int(11) NOT NULL,
  `recipe` int(11) NOT NULL,
  KEY `user` (`user`),
  KEY `recipe` (`recipe`),
  CONSTRAINT `saved_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`user_id`),
  CONSTRAINT `saved_ibfk_2` FOREIGN KEY (`recipe`) REFERENCES `recipes` (`recipe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saved`
--

LOCK TABLES `saved` WRITE;
/*!40000 ALTER TABLE `saved` DISABLE KEYS */;
INSERT INTO `saved` VALUES (27,3),(27,4),(25,5),(25,6),(24,6),(24,30);
/*!40000 ALTER TABLE `saved` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `user_id` int(11) NOT NULL,
  `cookie` text,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `user_id` (`user_id`),
  CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (24,'f9398e9e-5ae0-4032-909b-b8a6125e149d','2013-10-13 12:13:04'),(25,'d89c7b85-3e3b-4bc7-b0d1-ca43fa014b8c','2013-10-16 02:34:43'),(26,'8fa861aa-db34-4f77-a623-97f5773f98d0','2013-10-18 06:46:42'),(27,'ba44293c-b933-4f6f-b91c-0903eb2c3fef','2013-10-18 10:28:11'),(27,'6df5e488-2734-4b32-9dbf-90b9527d3b0d','2013-10-19 01:26:22'),(27,'fece6590-0ef8-4f5d-be26-61cca977af8c','2013-10-19 13:54:36'),(27,'b3e155fd-ad2b-4e67-b6f0-d97a5a5e0eb6','2013-10-20 02:01:01'),(27,'5088985e-b682-4f64-93cd-9a68f45a1af6','2013-10-20 05:32:17'),(27,'eefe52eb-f6a2-490f-8475-bf11408d0d8a','2013-10-20 05:36:13'),(28,'84b6774d-fcee-4bee-aab8-5bf51cd94d45','2013-10-20 11:56:33'),(25,'e584253d-28cd-4115-a6ba-34d41a6e8f0f','2013-10-21 12:56:01'),(28,'f0c70e2d-4d09-497f-aba1-5bae70e055c7','2013-10-21 12:56:19'),(29,'bcc551b6-d673-498d-a7da-532978c29123','2013-10-22 04:42:09'),(29,'aaf35845-6a67-403f-8643-a4a6cf33313e','2013-10-22 12:11:25'),(24,'b7d2460e-c56a-4d67-9a5b-6a84a6496cc9','2013-10-22 17:01:21');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unconfirmed`
--

DROP TABLE IF EXISTS `unconfirmed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unconfirmed` (
  `user_id` int(11) NOT NULL,
  `activation_code` text,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `user_id` (`user_id`),
  CONSTRAINT `unconfirmed_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unconfirmed`
--

LOCK TABLES `unconfirmed` WRITE;
/*!40000 ALTER TABLE `unconfirmed` DISABLE KEYS */;
INSERT INTO `unconfirmed` VALUES (26,'5edd0ae6-7400-4566-8b0f-e067071a6fd2','2013-10-18 06:44:51');
/*!40000 ALTER TABLE `unconfirmed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_confirmed` tinyint(1) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'testrapus','','','joey@tetrap.us',0,'Joey','Tuong'),(23,'o','e822fca2672923d33f008f8bdc9a1ca1b336156776ee95be620074580a7aa402','c231a105-24a7-44a0-a4e5-61366b631139','joey.tuong@gmail.com',1,'Joey',''),(24,'tetrapus','07e05d53d087319393471eea2f3faa8437e908b8b2eae2fa237fe74d035352c1','ee00be2a-3ead-460c-b8d4-afef66c6142a','deuterium.ion@gmail.com',1,'Joey','Tuong'),(25,'pgrasevski','10705b012cabbadee68c6020e8a1574463868130925d16e1d58bbccc5e66523c','e722ce7c-741b-45b4-90cb-b03e9f2764fe','peter.grasevski@gmail.com',1,'Peter','Grasevski'),(26,'finnegans','6f76e5e9443a69cc64fc6931550a17ca3d9bb78069f544f70bf7bb63dc1b0b56','4c297f61-c721-4878-8e43-fbe722d21918','finnegans.c@gmail.com',0,'Finnegans',''),(27,'Ice1','bb54a7a4703747dcbd618912590009800ce61739948a046c0134f82c43f6575d','22b6f565-15de-4322-9a09-337fd38fe38b','suphawit.ice@gmail.com',1,'suphawit55555','sappisarnkul'),(28,'finnegansy','07c4a360b8b696a3bbef0a52e9ec9d0fd863cef71e0a095e646fed55034241ca','62fac4bd-f34f-4952-b2da-8250f848e27a','finnegansy@gmail.com',1,'Finnegans',''),(29,'dickbutt','a98df36313325f2412e808dce7dfa36e5f713538c2648243401136e258f77a72','00edc35f-ff59-40d2-bbf1-f517ab715a11','crye@cse.unsw.edu.au',1,'pls','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-10-29 21:17:51
