Scrumbs is running live on http://scrumbs.tetrap.us:4920/.

Running the server:
1. Install Tomcat 7
2. Add the MySQL JDBC driver to your build path. This can be found at http://dev.mysql.com/downloads/connector/j/
3. Using eclipse:
	a. Install Eclipse EE
	b. Import the project
	c. Click run as > run on server.
	d. The site should now be live on http://localhost:8080/ScrumbsSystem/
   Using Apache:
    a. Find the path to the root of the webserver.
    b. Rename the project "ROOT.war"
    c. Restart tomcat (`service tomcat restart`)
    d. The site should now be live on http://localhost:8080/, or wherever tomcat is configured.

Running the database:
If you wish to run a local copy of the database, change WebContent/META-INF/context.xml to point to the local mysql server by:
1. Install MySQL Server.
2. Load the database dump into the local server using your credentials (`mysql -u <username> -p comp4920 < sql/create.sql`)
3. Update WebContent/META-INF/context.xml with the URL of your server and your credentials to the database.


Features:
Home Page & General UI: http://scrumbs.tetrap.us:4920/
	- Nav bar is responsive and the site looks beautiful on mobile.
	- Users who are logged in have access to content creation options.
	- Search is front-and-center on the homepage.
	- A random selection of highly-rated recipes are shown on the home page.
	- If you are logged in, unrated recipes are also shown to prompt you to try and rate them.
	- Invalid URLs get served a static 404 page- http://scrumbs.tetrap.us:4920/404

Recipe search: http://scrumbs.tetrap.us:4920/results
	- Main search bar searches title, description and category.
	- Advanced filters exist for most recipe fields. Click the "Add filters..." link to show the filters.
	- Filter fields understand the keywords AND, NOT and OR.
	- Ratings filter sorts the output by rating, and understands > and <
	- For logged in users, personal ratings are displayed on each recipe link.

User accounts: http://scrumbs.tetrap.us:4920/profile
	- Register and login buttons are visible for users who are not logged in.
	- Fields are properly validated. Once a user registers, they cannot log in until they follow a confirmation link emailed to them.
	- Users can log in via either email address or username.
	- Once logged in, sign up/in buttons are replaced with profile link and sign out link.
	- You can edit your own profile by clicking the edit profile button on your profile page.
	- You may view other user's profiles by clicking on their names where they appear- their email address is hidden.
	- Profiles show any rated, saved, or created recipes or meal plans
	
Recipe creation: http://scrumbs.tetrap.us:4920/createRecipe
	- Recipes may be created via the Create Recipe button on the nav visible to logged in users.
	- You may either upload an image or link to one found elsewhere on the internet.
	- Recipe creation can be done within other actions, such as viewing another recipe or editing a meal plan.
	- Your created recipe is visible on your profile page

View a recipe: http://scrumbs.tetrap.us:4920/recipe?recipeId=69
	- Basic recipe data is displayed nicely on the page.
	- If the recipe is a remix, a link to the parent recipe is visible under the title.
	- Clicking on an ingredient takes you to an advanced search for other recipes with that ingredient.
	- Clicking on the category takes you to an advanced search for other recipes in that category.
	- Comments are visible below the recipe, and display the user's rating for the recipe if they have rated it.
	- A list of remixes for the recipe are visible next to the comments, if any.
	- A list of up to 7 similar recipes (calculated based on ingredients, name, remix relationship and author) are displayed under the remixes.
	- A comment post box is visible below the comments if you are logged in.
	- If you are logged in, you may favourite, remix or rate the recipe. Performing any of these actions adds the recipe to your profile.
		- Your personal rating is shown as a number of stars next to the recipe image.
		- If you remix a recipe, a recipe creation dialog appears with all of the fields filled in. You may edit the recipe and save it as your own.
		- If you're viewing your own recipe, the remix button is replaced with a modify button, which additionally allows you to delete and update the recipe.
	- The global average rating is visible in search and on the recipe page.

Weekly plans: http://scrumbs.tetrap.us:4920/weeklyPlan
	- If you are logged out, you can view other users' weekly plans (via their profiles), or view a random/empty plan.
	- If you are logged in, you may save any of these plans to edit yourself.
	- If you are viewing someone else's plan, you may save it as your own.
	- All plans display a shopping list below the page showing all the ingredients necessary for a week. If it is a saved plan, a link appears to a printable version of the list.
	- Next to the shopping list, your own weekly plans and the other weekly plans of who you are viewing also appears, if applicable
	- If you are viewing your own weekly plan, you can edit it. The following operations are implemented:
		Via buttons on the entry (reloads page):
			- Delete entry (if exists)
			- Replace entry (replaces with first matching recipe)
			- Randomize entry (replaces with random recipe)
		Via dragging entries around:
			- Swap entries
			- Move entry into blank spot
		Via dragging recipes from the sidebar (hover over right-hand side to expose):
			- Replace entry
			- Move entry into blank spot
		Once any operation is performed, the plan is saved and the shopping list is updated.
		The sidebar contains the same recipes visible on your profile in condensed form.
	- You can create a recipe via the sidebar without reloading the page.
